# Car_auction


***************************Filezilla server ***********************
SERVER_IP_ADDRESS = 3.130.15.227

$ sudo ssh -i "nodebackend (1).pem" ubuntu@ec2-3-130-15-227.us-east-2.compute.amazonaws.com

$ cd ../../var/www/html

$  cd /project

$ sudo pm2 start server.js

$ sudo pm2 restart 19

$ sudo pm2 log 19

$ sudo rm -rf node_modules/   (remove file or folder)

$ cat app.js   (for see whole file)

**********************************************PM2 COMMANDS*************************************
# Fork mode
pm2 start app.js --name my-api # Name process

# Cluster mode
pm2 start app.js -i 0        # Will start maximum processes with LB depending on available CPUs
pm2 start app.js -i max      # Same as above, but deprecated.
pm2 scale app +3             # Scales `app` up by 3 workers
pm2 scale app 2              # Scales `app` up or down to 2 workers total

# Listing

pm2 list               # Display all processes status
pm2 jlist              # Print process list in raw JSON
pm2 prettylist         # Print process list in beautified JSON

pm2 describe 0         # Display all information about a specific process

pm2 monit              # Monitor all processes

# Logs

pm2 logs [--raw]       # Display all processes logs in streaming
pm2 flush              # Empty all log files
pm2 reloadLogs         # Reload all logs

# Actions

pm2 stop all           # Stop all processes
pm2 restart all        # Restart all processes

pm2 reload all         # Will 0s downtime reload (for NETWORKED apps)

pm2 stop 0             # Stop specific process id
pm2 restart 0          # Restart specific process id

pm2 delete 0           # Will remove process from pm2 list
pm2 delete all         # Will remove all processes from pm2 list

# Misc

pm2 reset <process>    # Reset meta data (restarted time...)
pm2 updatePM2          # Update in memory pm2
pm2 ping               # Ensure pm2 daemon has been launched
pm2 sendSignal SIGUSR2 my-app # Send system signal to script
pm2 start app.js --no-daemon
pm2 start app.js --no-vizion
pm2 start app.js --no-autorestart



*************************.ENV **********************************
PORT = 5000
MONGO_URL = mongodb://localhost:27017/car_auction

SERVICE = gmail

USER_MAIL = neha.mobulous@gmail.com

PASSWORD = etzdnwkgnppyhvkq

SECRET_KEY = neha_1234_yadav

BASE_PATH = /api/v1

*************************ADMIN CREDENTIALS********************
PASSWORD = Neha@123456

EMAIL = neha.yadav@mobulous.com

NAME = Neha Yadav