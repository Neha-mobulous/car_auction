const express = require('express');
const cors = require('cors');


require("dotenv").config();
const db = require("./db.connection/db");
const admin = require("./models/admin.model");
const routes = require("./routes/routes");


const PORT = process.env.PORT || 5000;
const app = express();

const socketio = require('socket.io');
const path = require('path');
app.use(express.static(path.resolve(__dirname, 'client')));
app.get('/', (req, res) => {
    console.log(__dirname);
    res.sendFile(path.resolve(__dirname, 'client', 'index.html'));
});

app.use(express.json());
app.use(cors());
app.use("/api/v1", routes);

const server = app.listen(PORT, (err) => {
    if (err) {
        console.log(`Error - ${err}`)
    } else {
        console.log(`Server running on port is ${PORT}`);
    }
})

let io = socketio(server)
const socketIo = require("./socket.io");
const { lastBidder, currentAllBidders } = require("./socket.bid");

io.on("connection", async (socket) => {

    socket.on("joinrooms", async (data) => {
        console.log(data, "room join");
        socket.join(data.roomId);

        socket.emit('joinrooms', data);
    });


    socket.on("sendMessage", async (data) => {
        console.log("message", data);

        await socketIo(data);

        io.to(data.roomId).emit("sendMessage", data);
    });


    socket.on('bid_Place', async (data) => {
        console.log("bid_Place", data);
        let result = await lastBidder(data);
        console.log(result, "result");
        io.emit("auction_Inprogress", result);
    });


    socket.on('Current_Auction_Bidders', async (data) => {
        console.log("Current_Auction_Bidders", data);
        let result = await currentAllBidders(data);
        console.log(result, "result");
        io.emit("Current_All_Bidders", result);
    });


    socket.on("joinRoomForAuction", async (data) => {
        console.log(data, "Auction room join");
        socket.join(data.auctionId);

        socket.emit('joinRoomForAuction', data);
    });

    socket.on("auctionMessage", async (data) => {
        console.log("auctionMessage", data);

        io.to(data.auctionId).emit("auctionMessage", data);
    });
});