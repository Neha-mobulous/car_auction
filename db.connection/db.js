const mongoose = require("mongoose");
mongoose.set('strictQuery', true)
const MONGO_URL = "mongodb://localhost:27017/car_auction";

mongoose.connect(MONGO_URL, { useNewUrlParser: true })
    .then(() => {
        console.log(`DB connection successfully!`);
    })
    .catch((err) => {
        console.log(`Error in db connection ${err}`);
    })