const mongoose = require("mongoose");

const saveproducts = new mongoose.Schema({
    productid: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'products'
    },
    userid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: true
    },
    status: {
        type: Boolean,
        default: true
    }
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model('saveproducts', saveproducts);