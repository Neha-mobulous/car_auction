const mongoose = require("mongoose");

const paymentHistory = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
    },
    paymentId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    paymentType: {
        type: String,
        enum: ['plan', 'auction']
    },
    transactionId: {
        type: String
    },
    amount: {
        type: String
    },
    paymentStatus: {
        type: String
    },
    description: {
        type: String
    }
},
    {
        timestamps: true
    })

module.exports = mongoose.model("paymenthistory", paymentHistory)
