const mongoose = require("mongoose");

const internetRetrieveFee = new mongoose.Schema({
    adminId: {
        type: String
    },
    InternetBidFee: {
        type: String,
        default: '00'
    },
    LotRetrievalFee: {
        type: String,
        default: '00'
    }
})

module.exports = mongoose.model('internet-retrieve-fee', internetRetrieveFee);