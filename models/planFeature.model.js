const mongoose = require("mongoose");

const Feature = new mongoose.Schema({
    feature: {
        type: String,
        required: true,
    },
    Delete: {
        type: Number,
        default: 0
    }
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model("planfeatures", Feature)