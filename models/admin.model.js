const mongoose = require("mongoose");
const { bcryptPass } = require("../utils/bcrypt.password");
const PASSWORD = "Neha@12345";
const EMAIL = "neha.yadav@mobulous.com";
const NAME = "Neha Yadav";
const ROLE = "admin"

const adminSchema = new mongoose.Schema({
    Name: {
        type: String
    },
    Email: {
        type: String
    },
    Password: {
        type: String
    },
    Role: {
        type: String,
        required: true
    },
    SubAdminId: {
        type: String
    },
    SubAdminRole: {
        type: String,
    },
    Mobile: {
        type: String
    },
    Login: {
        type: Boolean,
        default: true
    },
    OTP: {
        type: String
    },
    otpGeneratedAt: {
        type: String
    },
    Access: [
        {
            module: {
                type: String
            },
            access: {
                type: String,
                enum: ["1", "2", "3"],
                default: "3"
            },
        }
    ],
    Delete: {
        type: Number,
        default: 0
    },
    Status: {
        type: Boolean,
        default: true
    },
    token: {
        type: String
    }
},
    {
        timestamps: true
    });


const adminModel = mongoose.model("admin", adminSchema);

async function checkAdmin() {
    let Admin = await adminModel.exists({ Email: EMAIL });
    if (!Admin) {
        let hashPass = await bcryptPass(PASSWORD)
        await adminModel.create({ Name: NAME, Email: EMAIL, Password: hashPass, Role: ROLE });
    }
}
checkAdmin()


module.exports = adminModel;
