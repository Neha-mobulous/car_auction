const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema({
    CategoryName: {
        type: String,
        required: true
    },
    Delete: {
        type: Number,
        default: 0
    },
    Status: {
        type: Boolean,
        default: true
    }
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model('category', categorySchema);