const mongoose = require("mongoose");

const auctionSchema = new mongoose.Schema({
    AuctionId: {
        type: String,
        required: true
    },
    CategoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "categories",
        required: true
    },
    SubCategoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "subcategories",
        required: true
    },
    ProductId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "products",
        required: true
    },
    SellerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: true
    },
    Location: {
        type: { type: String, default: 'Point', },
        coordinates: [Number]
    },
    Place: {
        type: String,
        required: true
    },
    City: {
        type: String,
        required: true
    },
    Country: {
        type: String,
        required: true
    },
    Model: {
        type: String,
        required: true
    },
    Quantity: {
        type: String,
        required: true
    },
    Lot_description: {
        type: String,
        required: true
    },
    StartDate: {
        type: String,
        required: true
    },
    StartTime: {
        type: String,
        required: true
    },
    Duration: {
        type: String,
        required: true
    },
    Bid_start_rate: {
        type: String
    },
    Reserve_price: {
        type: String
    },
    AuctionType: {
        type: String,
        required: true
        // On Reserve or Pure Sale
    },
    Delete: {
        type: Number,
        default: 0
    },
    AdminVerify: {
        type: String,
        default: "pending",
        enum: ["pending", "rejected", "approved"]
    },
    AuctionStatus: {
        type: String,
        enum: ["inprogress", "upcoming", "completed"],
        default: "upcoming"
    }
},
    {
        timestamps: true
    });

module.exports = mongoose.model('auction', auctionSchema);