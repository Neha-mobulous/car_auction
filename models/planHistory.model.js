const mongoose = require("mongoose");

const planhistory = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: true
    },
    planHistory: {
        PlanName: {
            type: String,
            required: true,
        },
        Plan_price: {
            type: String,
            required: true
        },
        ValidTill: {
            type: String,
            required: true
        },
        Number_Auction: {
            type: Number
        },
        description: {
            type: String,
        },
        planStatus: {
            type: Boolean,
            default: true
        },
        Delete: {
            type: Number,
            default: 0
        },
        UserType: {
            type: String,
            enum: ['buyer', 'seller'],
            default: 'buyer'
        },
        Access: [
            {
                module: {
                    type: String
                },
                access: {
                    type: String,
                    enum: ["1", "2", "3"],
                    default: "1"
                },
            }
        ],
    }
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model('planhistory', planhistory);