const mongoose = require("mongoose");

const subcategorySchema = new mongoose.Schema({
    SubCategoryName: {
        type: String,
        required: true
    },
    CategoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'categories',
        required: true
    },
    Delete: {
        type: Number,
        default: 0
    },
    Status: {
        type: Boolean,
        default: true
    },
    Attribute: {
        type: Array
    }
},
    {
        timestamps: true
    })

module.exports = mongoose.model('subcategory', subcategorySchema);