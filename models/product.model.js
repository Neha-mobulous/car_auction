const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    ProductName:
    {
        type: String,
        required: true
    },
    ProductType: {
        type: String,
        required: true
    },
    Model: {
        type: String,
    },
    Attribute: {
        type: Array
    },
    CategoryId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "categories"
    },
    SubCategoryId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "subcategories"
    },
    BuyerCommission: {
        type: String
    },
    SellerCommission: {
        type: String
    },
    Location: {
        type: { type: String, default: 'Point', },
        coordinates: [Number]
    },
    Place: {
        type: String
    },
    City: {
        type: String
    },
    Country: {
        type: String
    },
    State: {
        type: String
    },
    description:
    {
        type: String
    },
    Delete: {
        type: Number,
        default: 0
    },
    Status: {
        type: Boolean,
        default: true
    },
    Photos: [
        {
            image: {
                type: String
            }
        }
    ],
    SellerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    AdminVerify: {
        type: String,
        default: "pending",
        enum: ["pending", "rejected", "approved"]
    },
    CreatedBy: {
        type: String
    }
},
    {
        timestamps: true
    })

module.exports = mongoose.model('products', productSchema);