const mongoose = require("mongoose");

const bid = new mongoose.Schema({
    auctionid: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'auctions'
    },
    bidPrice: {
        type: String
    },
    userid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: true
    },
    bidStatus: {
        type: String
    },
    roomId: {
        type: String
    }
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model('bid', bid);