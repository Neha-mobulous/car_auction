const mongoose = require('mongoose')
const Schema = mongoose.Schema

const collection = new Schema({
    senderId: {
        type: mongoose.Types.ObjectId,
        ref: "users",
    },
    receiverId: {
        type: mongoose.Types.ObjectId,
        ref: "users",
    },
    roomId: {
        type:String
    },
    chatId:{
        type: String,
        ref: "chats",
    },
    message: {
        type: String,
        default: ''
    },
    attachment: {
        type: Array
    }
}, { timestamps: true })

module.exports = mongoose.model("chats", collection)