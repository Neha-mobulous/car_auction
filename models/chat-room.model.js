const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const collection = new Schema({
    senderId: {
        type: mongoose.Types.ObjectId,
        ref: "users",
    },
    receiverId: {
        type: mongoose.Types.ObjectId,
        ref: "users",
    },
    roomId: {
        type: String
    },
    auctionId: {
        type: mongoose.Types.ObjectId,
        ref: "auctions",
    },
    lastmessage: {
        type: String,
        default: ''
    },
    title: {
        type: String,
        default: ''
    },
    startTime: {
        type: String
    },
    endTime: {
        type: String
    },
    requestDate: [{
        type: String
    }],
    chatCount: {
        type: Number
    },
    delete_by: {
        type: Array
    },
    is_delete: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true
})

module.exports = mongoose.model('chat-rooms', collection);
