const mongoose = require("mongoose");

const planModel = new mongoose.Schema({
    PlanName: {
        type: String,
        required: true,
    },
    Plan_price: {
        type: String,
        required: true
    },
    Plan_icon: {
        type: String
    },
    ValidTill: {
        type: String,
        required: true
    },
    Number_Auction: {
        type: Number
    },
    description: {
        type: String,
    },
    Status: {
        type: Boolean,
        default: true
    },
    Delete: {
        type: Number,
        default: 0
    },
    UserType: {
        type: String,
        enum: ['buyer', 'seller'],
        default: 'buyer'
    },
    Access: [
         
    ],
},
    {
        timestamps: true
    })

module.exports = mongoose.model('plan', planModel);