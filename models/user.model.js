const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    userType: {
        type: String,
        enum: ["buyer", "seller"],
        default: "buyer"
    },
    Fullname: {
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true,
        unique: true
    },
    Mobile: {
        type: String
    },
    Profile: {
        type: String,
        default: ""
    },
    Password: {
        type: String,
        required: true
    },
    Address: [{
        Region: { type: String },
        Sub_city: { type: String },
        Wereda: { type: String },
        Kebele: { type: String },
        House_number: { type: String }
    }],
    Payment_method: {
        type: String
    },
    DebitCreditCard: [
        {
            CardNumber: {
                type: String
            },
            ExpiresEnd: {
                type: String
            },
            CVV: {
                type: String
            }
        }
    ],
    Logout: {
        type: Boolean,
        default: false
    },
    Notifications: {
        MyAccount: {
            type: Boolean,
            default: false
        },
        LiveAuction: {
            type: Boolean,
            default: false
        },
        MyBids: {
            type: Boolean,
            default: false
        },
        MyRecomendation: {
            type: Boolean,
            default: false
        },
    },
    Language: {
        type: String,
        enum: ["English", "Amharic", "Oromo"],
        default: "English"
    },
    OTP: {
        type: String
    },
    otpGeneratedAt: {
        type: Date,
        default: new Date()
    },
    Contact_Us: [{
        Name: { type: String },
        Email: { type: String },
        Subject: { type: String },
        Message: { type: String }
    }],
    token: {
        type: String
    },
    Documents: [
        {
            Driving_License: {
                front: {
                    type: String
                },
                back: {
                    type: String
                }
            },
            Aadhar_Card: {
                front: {
                    type: String
                },
                back: {
                    type: String
                }
            }
        }
    ],
    verifySeller: {
        type: String,
        enum: ['0', '1', '2', '3'],
        default: '0'
    },
    Status: {
        type: Boolean,
        default: true
    },
    Delete: {
        type: Number,
        default: 0
    },
    Plan: {
        PlanName: {
            type: String,
        },
        Plan_price: {
            type: String,
        },
        ValidTill: {
            type: String,
        },
        Number_Auction: {
            type: Number
        },
        description: {
            type: String,
        },
        Status: {
            type: Boolean,
            default: true
        },
        Delete: {
            type: Number,
            default: 0
        },
        UserType: {
            type: String,
            enum: ['buyer', 'seller'],
            default: 'buyer'
        },
        Access: [
            {
                module: {
                    type: String
                },
                access: {
                    type: String,
                    enum: ["1", "2", "3"],
                    default: "1"
                },
            }
        ],
        PaymentStatus: {
            type: String,
            enum: ['pending', 'success', 'failed'],
        },
        Transactionid: {
            type: String,
        },
        PaymentPlanDate: {
            type: String,
        }
    }
},
    {
        timestamps: true
    });

module.exports = mongoose.model("users", userSchema)