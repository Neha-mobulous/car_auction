const userModel = require("../../models/user.model");

const Logout = async (req, res) => {
    let token = req.headers.authorization;
    try {
        let result = await userModel.findOneAndUpdate({ token }, { Logout: true, token: "" });
        return res.status(200).send({ message: "Logout successfully!", code: 200 });

    } catch (error) {
        console.log(error);
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { Logout };