const userModel = require("../../models/user.model");
const { addDebitCredit } = require("../../schemas/debitCreditCard");
const { bcryptPass } = require("../../utils/bcrypt.password");

const AddDebitCreditCard = async (req, res) => {
    const { value, error } = addDebitCredit.validate(req.body);
    const token = req.headers.token;

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let bcryptCardNumber = await bcryptPass(value.CardNumber);
        value.CardNumber = bcryptCardNumber;
        let ress = await userModel.findOneAndUpdate({ token }, { DebitCreditCard: value });
        return res.status(200).send({ message: "Card added successfully!", code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { AddDebitCreditCard }