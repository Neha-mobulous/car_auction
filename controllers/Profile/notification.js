const userModel = require("../../models/user.model");
const { NotificationSchema } = require("../../schemas/notificationSchema");

const Notification = async (req, res) => {
    const token = req.headers.authorization;
    const { value, error } = NotificationSchema.validate(req.body);

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let dbResult = await userModel.findOneAndUpdate({ token }, { Notifications: value });
        return res.status(200).send({ message: "Notifications have changed!", code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { Notification }