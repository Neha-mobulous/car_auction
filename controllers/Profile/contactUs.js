const userModel = require("../../models/user.model");
const { contactUs } = require("../../schemas/contactUs.schema");

const contactWithAdmin = async (req, res) => {
    const { value, error } = contactUs.validate(req.body);
    let token = req.headers.authorization;

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let result = await userModel.findOneAndUpdate({ token: token }, { Contact_Us: [value] });
        return res.status(200).send({ message: "Query Submitted succesfully!", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { contactWithAdmin }