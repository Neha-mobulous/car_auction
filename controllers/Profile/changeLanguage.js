const userModel = require("../../models/user.model");

const changeLanguage = async (req, res) => {
    const token = req.headers.authorization;
    const Language = req.body.language;

    if (!Language) {
        return res.status(400).send({ message: "language required !", code: 400 });
    }

    try {
        let dbResult = await userModel.findOneAndUpdate({ token: token }, { Language });
        return res.status(200).send({ message: "Language has changed successfully!", code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { changeLanguage }