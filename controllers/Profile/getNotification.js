const userModel = require("../../models/user.model");

const getNotifications = async (req, res) => {
    const token = req.headers.authorization;
    try {
        let dbResult = await userModel.findOne({ token: token });
        return res.status(200).send({ notifications: dbResult.Notifications });
    }
    catch (error) {
        console.log(error);
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { getNotifications }