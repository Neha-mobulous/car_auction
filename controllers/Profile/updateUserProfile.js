const userModel = require("../../models/user.model");


const updateUserProfile = async (req, res) => {
    let value = req.body;
    const token = req.headers.authorization;

    if (!value) {
        return res.status(400).send({ message: "value required!", code: 400 });
    }

    try {
        let DbDetails = await userModel.findOneAndUpdate({ token: token }, value);
        if (!DbDetails) {
            return res.status(400).send({ message: "Wrong Details!", code: 400 });
        }
        return res.status(200).send({ message: "User Profile changed successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 })
    }
}

module.exports = { updateUserProfile }