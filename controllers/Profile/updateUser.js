const userModel = require("../../models/user.model");

const UpdateUserType = async (req, res) => {
    let token = req.headers.authorization;
    let userType = req.body.userType;

    if (!userType) {
        return res.status(400).send({ message: "userType or token required!", code: 400 });
    }

    try {
        let DbDetails = await userModel.findOneAndUpdate({ token: token }, { userType }, { new: true });
        console.log(DbDetails, "******************userType changed****************");
        if (!DbDetails) {
            return res.status(400).send({ message: "Wrong Details!", code: 400 });
        }
        return res.status(200).send({ message: "userType changed successfully!", code: 200 });

    } catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { UpdateUserType };