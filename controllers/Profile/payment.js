const { ObjectId } = require("mongodb");
const paymentHistoryModel = require("../../models/paymentHistory.model");
const userModel = require("../../models/user.model");
const planHistoryModel = require("../../models/planHistory.model");
const auctionModel = require("../../models/auction.model");



//get payment api
const getPaymentHistory = async (req, res) => {
    let token = req.headers.authorization;
    try {
        let user = await userModel.findOne({ token });
        let getHistory = await paymentHistoryModel.find({ userId: ObjectId(user._id) });

        console.log(getHistory.length);
        let paymentHistory = [];
        for (let planOrAuction of getHistory) {
            if (planOrAuction.paymentType == 'plan') {
                let planDetail = await planHistoryModel.findById({ _id: planOrAuction.paymentId });
                let a = planOrAuction;
                // a.paymentType = planOrAuction.paymentType;
                a.payHistory = planDetail;
                console.log(a);
                paymentHistory.push(a);
            } else {
                let auctionDetail = await auctionModel.findById({ _id: planOrAuction.paymentId });
                let a = planOrAuction;
                // a.paymentType = planOrAuction.paymentType;
                a.payHistory = auctionDetail;
                console.log(a);
                paymentHistory.push(a);
            }
        }
        return res.send({ paymentHistory, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}



module.exports = { getPaymentHistory }