const userModel = require("../../models/user.model");
const { changePasswordSchema } = require("../../schemas/changepassword.schema");
const { validatePassword, bcryptPass } = require("../../utils/bcrypt.password");

const changePassword = async (req, res) => {
    const { value, error } = changePasswordSchema.validate(req.body);
    const token = req.headers.authorization;

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 })
    }

    try {
        let dbResult = await userModel.findOne({ token: token });
        let comparePash = await validatePassword(value.CurrentPassword, dbResult.Password);
        if (comparePash) {
            let hashPass = await bcryptPass(value.NewPassword);
            await userModel.findOneAndUpdate({ token: token }, { Password: hashPass });
            return res.status(200).send({ message: "Your password has successfully changed!", code: 200 });
        } else {
            return res.status(400).send({ message: "Current Password wrong!", code: 400 });
        }

    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { changePassword }