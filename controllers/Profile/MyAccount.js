const userModel = require("../../models/user.model");

const MyAccount = async (req, res) => {
    let token = req.headers.authorization;
    
    try {
        let myDetail = await userModel.findOne({ token: token });
        return res.status(200).send(myDetail);
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { MyAccount }