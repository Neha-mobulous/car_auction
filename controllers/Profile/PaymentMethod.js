const userModel = require("../../models/user.model");

const AddPaymentMethod = async (req, res) => {
    const Payment_method = req.body.Payment_method;
    const token = req.headers.token;
    if (!Payment_method) {
        return res.status(400).send({ message: "payment method required !", code: 400 });
    }

    try {
        let dbResult = await userModel.findOneAndUpdate({ token }, { Payment_method });
        return res.status(200).send({ message: "Payment Method added successfully!", code: 200 })
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { AddPaymentMethod }