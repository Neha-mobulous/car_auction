const bidModel = require("../../models/bid.model");

const getBidDetailsForSeller = async (req, res) => {
    let auctionid = req.body.auctionid;
    if (!auctionid) {
        return res.send({ message: "auctionid is required!", code: 400 })
    }
    try {
        let auctionDetail = await bidModel.find({ auctionid: auctionid });
        if (!auctionDetail) {
            return res.send({ message: "auctionid not exist!", code: 400 });
        }
        return res.send({ message: auctionDetail, code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}
module.exports = { getBidDetailsForSeller }