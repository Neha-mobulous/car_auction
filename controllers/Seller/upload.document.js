const userModel = require("../../models/user.model");

const uploadDocuments = async (req, res) => {
    let token = req.headers.authorization;
    let value = req.body;
    if (!token || !value) {
        return res.status(400).send({ message: "token documents required!", code: 400 });
    }

    try {
        console.log(value);
        let dbResult = await userModel.findOneAndUpdate({ token: token }, { $push: { Documents: value } });
        let updateDocumentSatusResult = await userModel.findOneAndUpdate({ token: token }, { verifySeller: "1" });
        if (!dbResult || !updateDocumentSatusResult) {
            return res.status(400).send({ message: "wrong token!", code: 400 });
        }
        return res.status(200).send({ message: "Document uploaded successfully!", code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { uploadDocuments }