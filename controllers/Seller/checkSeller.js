const userModel = require("../../models/user.model");

const verifySellerFromAdmin = async (req, res) => {
    let token = req.headers.authorization;
    if (!token) {
        return res.status(400).send({ message: "token required!", code: 400 });
    }

    try {
        let dbResult = await userModel.findOne({ token });
        if (!dbResult) {
            return res.status(400).send({ message: "user not exist!", code: 400 });
        }

        if (dbResult.verifySeller === '1') {
            return res.status(200).send({ message: "pending", code: 200 });
        } else if (dbResult.verifySeller === '2') {
            return res.status(200).send({ message: "verified", code: 200 });
        } else if (dbResult.verifySeller === '3') {
            return res.status(200).send({ message: "rejected", code: 200 });
        } else {
            return res.status(200).send({ message: "new", code: 200 });
        }
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { verifySellerFromAdmin }