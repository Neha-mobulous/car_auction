const planModel = require("../../models/plan.model");
const { PlanSchema } = require("../../schemas/plan.schema");
const featureModel = require("../../models/planFeature.model");


const createFeature = async (req, res) => {
    const feature = req.body.feature;
    if (!feature) {
        return res.send({ message: "feature is required!", code: 400 })
    }
    try {
        await featureModel.create({ feature: feature });
        return res.send({ message: "feature created successfully!", code: 200 })
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const getFeatureOfPlan = async (req, res) => {
    try {
        let features = await featureModel.find({ Delete: 0 });
        return res.send({ features, code: 200 })
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const createPlan = async (req, res) => {
    const { value, error } = PlanSchema.validate(req.body);

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }
    try {
        await planModel.create(value);
        return res.send({ message: "plan created successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const PlanListing = async (req, res) => {
    const page = parseInt(req.body.page) || 1;
    const limit = parseInt(req.body.limit) || 10;
    const skip = (page - 1) * limit;
    const searchText = req.body.searchText || "";

    let searchMatch = {};
    if (searchText) {
        searchMatch.$or = [
            {
                "PlanName": {
                    $regex: searchText,
                    $options: 'i'
                }
            },
            {
                "UserType": {
                    $regex: searchText,
                    $options: 'i'
                }
            }
        ]
    }
    try {
        let planResult = await planModel.find({ ...searchMatch, Delete: 0 }).skip(skip).limit(limit).sort({ createdAt: -1 });
        return res.send({ result: planResult, total: planResult.length, code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const updatePlan = async (req, res) => {
    const planid = req.body.planid;
    const value = req.body;
    if (!planid) {
        return res.send({ message: "plan id required!", code: 400 });
    }
    try {
        let dbResult = await planModel.findByIdAndUpdate({ _id: planid }, value, { new: true });
        if (!dbResult) {
            return res.send({ message: "plan id not exist!", code: 400 });
        }
        return res.send({ message: "Plan updated successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const deletePlan = async (req, res) => {
    const planid = req.body.planid;
    if (!planid) {
        return res.send({ message: "plan id required!", code: 400 });
    }
    try {
        let dbResult = await planModel.findByIdAndUpdate({ _id: planid }, { Delete: 1 }, { new: true });
        if (!dbResult) {
            return res.send({ message: "plan id not exist!", code: 400 });
        }
        return res.send({ message: "plan deleted successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const updatePlanStatus = async (req, res) => {
    const status = req.body.status;
    const planid = req.body.planid;
    if (!planid) {
        return res.send({ message: "plan id required!", code: 400 });
    }
    if (status == undefined) {
        return res.send({ message: "status is requried!", code: 400 })
    }
    try {
        let dbResult = await planModel.findByIdAndUpdate({ _id: planid }, { Status: status });
        if (!dbResult) {
            return res.send({ message: "Plan id not exist!", code: 400 });
        }
        return res.send({ message: "Plan status has changed successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}



module.exports = { createFeature, getFeatureOfPlan, createPlan, PlanListing, updatePlan, deletePlan, updatePlanStatus }