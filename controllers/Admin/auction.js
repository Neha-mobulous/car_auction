const auctionModel = require("../../models/auction.model");
const { ObjectId } = require('mongodb');

const GetAuctionList = async (req, res) => {
    const page = parseInt(req.body.page) || 1;
    const limit = parseInt(req.body.limit) || 10;
    const skip = (page - 1) * limit;
    const searchText = req.body.searchText || "";
    try {
        let auctionResult = await auctionModel.aggregate([
            { '$match': { "Delete": 0 } },
            { "$sort": { createdAt: -1 } },
            {
                '$lookup': {
                    'from': 'users',
                    'localField': 'SellerId',
                    'foreignField': '_id',
                    'as': 'userDetails'
                }
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'ProductId',
                    'foreignField': '_id',
                    'as': 'product'
                }
            },
            {
                '$match': {
                    '$or': [
                        { "AuctionId": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                        { "userDetails.Fullname": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                    ]
                }
            },
            { '$skip': skip },
            { '$limit': limit },
        ])
        let count = await auctionModel.countDocuments({ ...searchText, Delete: 0 });
        return res.send({ auctionResult, total: count, code: 200 })

    } catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const DeleteAuctionFromAdmin = async (req, res) => {
    const auctionid = req.body.auctionid;
    if (!auctionid) {
        return res.send({ message: "auction id required!", code: 400 });
    }
    try {
        let dbResult = await auctionModel.findOneAndUpdate({ AuctionId: auctionid }, { Delete: 1 }, { new: true });
        if (!dbResult) {
            return res.send({ message: "auction id not exist!", code: 400 });
        }
        return res.send({ message: "auction deleted successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const AuctionDetailFromAdmin = async (req, res) => {
    const auctionid = req.body.auctionid;
    if (!auctionid) {
        return res.send({ message: "auction id required!", code: 400 });
    }
    try {

        let auctionResult = await auctionModel.aggregate([
            {
                $match: { _id: ObjectId(auctionid) },
            },
            { '$match': { "Delete": 0 } },
            {
                '$lookup': {
                    'from': 'users',
                    'localField': 'SellerId',
                    'foreignField': '_id',
                    'as': 'userDetails'
                }
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'ProductId',
                    'foreignField': '_id',
                    'as': 'product'
                }
            },
        ])
        if (!auctionResult) {
            return res.send({ message: "auction id not exist!", code: 400 });
        }
        return res.send({ result: auctionResult, code: 200 });
    }
    catch (error) {
        return res.send({ Error, error, code: 500 });
    }

}

const verifyAuction = async (req, res) => {
    const verifystatus = req.body.verifystatus;
    const auctionid = req.body.auctionid;
    if (!auctionid || verifystatus == undefined) {
        return res.send({ message: "auction id and verifystatus required!", code: 400 });
    }
    try {
        let dbResult = await auctionModel.findByIdAndUpdate({ _id: auctionid }, { AdminVerify: verifystatus }, { new: true });
        if (!dbResult) {
            return res.send({ message: "Auction id not exist!", code: 400 });
        }
        return res.send({ message: "Auction verify successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { GetAuctionList, DeleteAuctionFromAdmin, AuctionDetailFromAdmin, verifyAuction }