const userModel = require("../../models/user.model");

const sellerVerify = async (req, res) => {
    const userId = req.body.id;
    const key = req.body.key;

    if (!userId) {
        return res.status(400).send({ message: "userid required!", code: 400 });
    }
    try {
        let userResult = await userModel.findByIdAndUpdate({ _id: userId }, { verifySeller: key, userType: "seller" });
        if (!userResult) {
            return res.send({ message: "user does not exist", code: 400 });
        }
        return res.status(200).send({ message: "seller verified", code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { sellerVerify }