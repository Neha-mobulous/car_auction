const adminModel = require("../../models/admin.model");

const adminLogout = async (req, res) => {
    const token = req.headers.authorization;

    try {
        let result = await adminModel.findOneAndUpdate({ token }, { Login: false, token: "" });
        return res.status(200).send({ message: "Logout successfully!", code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { adminLogout }