const userModel = require("../../models/user.model");
const auctionModel = require("../../models/auction.model");
const productModel = require("../../models/product.model");
const internetRetrieveFeeModel = require("../../models/internetRetrieve.model");
const moment = require("moment");
const adminModel = require("../../models/admin.model");


const Dashboard = async (req, res) => {
    try {
        let search = { Delete: 0 }
        if (!req.body.startDate == '' && !req.body.endDate == "") {
            let endDate = new Date(req.body.endDate)
            endDate.setDate(endDate.getDate() + 1)
            search.createdAt = {
                $gte: new Date(req.body.startDate),
                $lte: endDate
            }
            console.log(endDate, new Date(req.body.startDate));
        }
        if (req.body.timeFrame == "Today") {
            let setTodayDate = new Date()
            setTodayDate.setHours(0, 0, 59, 999);
            let todayDate = new Date()
            todayDate.setHours(23, 59, 59, 999);
            search.createdAt = {
                $gte: setTodayDate,
                $lte: todayDate
            }
        }
        if (req.body.timeFrame == "Week") {
            let weekDate = new Date(moment().startOf('isoWeek').format())
            let todayDate = new Date()
            todayDate.setHours(23, 59, 59, 999);
            search.createdAt = {
                $gte: weekDate,
                $lte: todayDate
            }
        }
        if (req.body.timeFrame == "Month") {
            let monthDate = new Date(moment().clone().startOf('month').format())
            let todayDate = new Date()
            todayDate.setHours(23, 59, 59, 999);
            search.createdAt = {
                $gte: monthDate,
                $lte: todayDate
            }
        }
        let TotalUsers = await userModel.find({ ...search }).countDocuments();
        let totalSellers = await userModel.find({ ...search, userType: "seller" }).countDocuments();
        let totalAuctions = await auctionModel.find({ ...search }).countDocuments();
        let totalProducts = await productModel.find({ ...search }).countDocuments();
        let allProduct = await productModel.find({});
        let totalCommission = 0
        for (product of allProduct) {
            if (product.BuyerCommission && product.SellerCommission) {
                totalCommission = totalCommission + (Number(product.SellerCommission) + Number(product.BuyerCommission))
            }
        }

        if (TotalUsers || totalSellers || totalAuctions || totalProducts) {
            let data = {
                TotalUsers: TotalUsers || 0,
                totalSellers: totalSellers || 0,
                totalAuctions: totalAuctions || 0,
                totalProducts: totalProducts || 0,
                totalCommission: totalCommission || 0
            }
            return res.send({ result: data, code: 200 })
        } else {
            let data = {
                TotalUsers: 0,
                totalSellers: 0,
                totalAuctions: 0,
                totalProducts: 0,
                totalCommission: 0
            }
            return res.send({ result: data, code: 200 })

        }
    } catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }

}


const createInternetAndRetrieval = async (req, res) => {
    let token = req.headers.authorization;
    let InternetBidFee = '00';
    let LotRetrievalFee = '00';
    try {
        let admin = await adminModel.findOne({ token });
        await internetRetrieveFeeModel.create({ adminId: admin._id, InternetBidFee, LotRetrievalFee });
        return res.send({ message: "InternetBidFee, LotRetrievalFee created succesfully!", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}

const updateInternetAndRetrieval = async (req, res) => {
    let token = req.headers.authorization;
    let { InternetBidFee, LotRetrievalFee } = req.body;

    if (!InternetBidFee || !LotRetrievalFee) {
        return res.send({ message: "InternetBidFee, LotRetrievalFee required!", code: 400 });
    }
    try {
        let admin = await adminModel.findOne({ token });
        await internetRetrieveFeeModel.findOneAndUpdate({ adminId: admin._id }, { InternetBidFee: InternetBidFee, LotRetrievalFee: LotRetrievalFee });
        return res.send({ message: "update LotRetrievalFee and InternetBidFee suceesfully!", code: 200 })
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}

const getInternetAndRetrieval = async (req, res) => {
    try {
        let internetRetrieveFee = await internetRetrieveFeeModel.find();
        return res.send({ result: internetRetrieveFee, code: 200 })
    } catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { Dashboard, createInternetAndRetrieval, updateInternetAndRetrieval, getInternetAndRetrieval }