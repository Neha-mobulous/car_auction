const adminModel = require("../../models/admin.model");
const { subAdminSchema } = require("../../schemas/subAdminSchema");
const { bcryptPass } = require("../../utils/bcrypt.password");

const CreateSubAdmin = async (req, res) => {
    const { value, error } = subAdminSchema.validate(req.body);

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }
    try {
        let hashPass = await bcryptPass(value.Password);
        value.Password = hashPass;

        let countSubAdmin = await adminModel.find({ Role: "SubAdmin", Delete: 0 }).countDocuments();
        let dbResultfind = await adminModel.findOne({ 'Email': value.Email });
        if (dbResultfind) {
            return res.send({ message: "SubAdmin already exist.", code: 400 });
        }
        value.SubAdminId = `SUB-00${countSubAdmin + 1}`;
        value.Role = 'SubAdmin';
        await adminModel.create(value);
        return res.status(200).send({ message: "Sub Admin created successfully!", code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const DeleteSubAdmin = async (req, res) => {
    const subadminid = req.body.subadminid // sub admin id

    if (!subadminid) {
        return res.status(400).send({ message: "sub admin id required!", code: 400 });
    }

    try {
        let subadminUser = await adminModel.findById({ _id: subadminid });
        if (!subadminUser) {
            return res.send({ message: "subadmin id not exist.", code: 400 });
        }
        await adminModel.findByIdAndUpdate({ _id: subadminid }, { Email: `delete${subadminUser.Email}`, Delete: 1 }, { new: true });
        return res.status(200).send({ message: "Sub Admin deleted successfully!", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const EditSubAdmin = async (req, res) => {
    const id = req.body.id // sub admin id
    const value = req.body;

    if (!id) {
        return res.status(400).send({ message: "sub admin id required!", code: 400 });
    }

    try {
        let dbResult;
        console.log(value, "***************************");
        if (value.Password != '') {
            let hashPass = await bcryptPass(value.Password);
            value.Password = hashPass;
            dbResult = await adminModel.findByIdAndUpdate({ _id: id }, value, { new: true });
        }
        else {
            dbResult = await adminModel.findByIdAndUpdate({ _id: id }, value, { new: true });
        }
        if (!dbResult) {
            return res.send({ message: "Id not exist.", code: 400 });
        }
        return res.status(200).send({ message: "Sub Admin updated successfully!", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}


const getSubAdmin = async (req, res) => {
    const page = parseInt(req.body.page) || 1;
    const limit = parseInt(req.body.limit) || 10;
    const skip = (page - 1) * limit;
    const searchText = req.body.searchText || "";
    try {
        const list = await adminModel.aggregate([
            { '$match': { "Delete": 0 } },
            { '$match': { "Role": 'SubAdmin' } },
            {
                '$match': {
                    '$or': [
                        { "SubAdminRole": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                        { "Email": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                        { "Name": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                        { "SubAdminId": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                    ]
                }
            },
            { '$skip': skip },
            { '$limit': limit },
            { '$sort': { createdAt: -1 } },
        ])
        return res.send({ list, total: list.length, code: 200 });
    } catch (error) {
        console.log(error);
        return res.status(500).send({ Error: error, code: 500 });

    }
}


const SubAdminRoleAccess = async (req, res) => {
    const subadminId = req.body.subadminid;

    if (!subadminId) {
        return res.status(400).send({ message: "Subadmin id are required!", code: 400 });
    }
    try {
        let dbResult = await adminModel.find({ _id: subadminId });
        if (!dbResult || dbResult[0].Access.length === 0) {
            return res.status(400).send({ message: "Sub admin or Access details are not exist!", code: 400 });
        }
        return res.status(200).send({ result: dbResult[0].Access, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}



const updatSubAdminStatus = async (req, res) => {
    const subAdminId = req.body.subadminid;
    const Status = req.body.Status;

    if (!subAdminId) {
        return res.status(400).send({ message: "subadminId required!", code: 400 });
    }
    if (Status == undefined) {
        return res.status(400).send({ message: "Status is required!", code: 400 });
    }

    try {
        let subAdminResult = await adminModel.findOneAndUpdate({ '_id': subAdminId }, { "$set": { "Status": Status } }, { new: true });
        if (!subAdminResult) {
            return res.send({ message: "Sub Admin id not exist!", code: 400 });
        }
        return res.status(200).send({ message: "Sub Admin status changed successfully!", code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { CreateSubAdmin, DeleteSubAdmin, EditSubAdmin, getSubAdmin, SubAdminRoleAccess, updatSubAdminStatus }