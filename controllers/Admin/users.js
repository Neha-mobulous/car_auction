const userModel = require("../../models/user.model");

const getUsersList = async (req, res) => {
    const page = parseInt(req.body.page) || 1;
    const limit = parseInt(req.body.limit) || 10;
    const skip = (page - 1) * limit;
    const searchText = req.body.searchText || "";

    let searchMatch = {};
    if (searchText) {
        searchMatch.$or = [
            {
                "Fullname": {
                    $regex: searchText,
                    $options: 'i'
                }
            },
            {
                "Email": {
                    $regex: searchText,
                    $options: 'i'
                }
            },
            {
                "Mobile": {
                    $regex: searchText,
                    $options: 'i'
                }
            }
        ]
    }
    try {
        let result = await userModel.find({ ...searchMatch, Delete: 0 }).skip(skip).limit(limit).sort({ createdAt: -1 });
        let count = await userModel.countDocuments({ ...searchMatch, Delete: 0 });
        return res.send({ result, total: count, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const updateStatusOfUser = async (req, res) => {
    const status = req.body.status;
    const userid = req.body.userid;
    if (!userid) {
        return res.status(400).send({ message: "userid required!", code: 400 });
    }
    if (status == undefined) {
        return res.send({ message: "status is required!", code: 400 })
    }
    try {
        let result = await userModel.findByIdAndUpdate({ _id: userid }, { Status: status });
        if (!result) {
            return res.send({ message: "userid wrong!" })
        }
        return res.send({ message: "User status has changed successfully!", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}

const deleteUserFromAdmin = async (req, res) => {
    const userid = req.body.userid;
    if (!userid) {
        return res.status(400).send({ message: "userid required!", code: 400 });
    }
    try {
        let foundUser = await userModel.findById({ _id: userid });
        if (!foundUser) {
            return res.send({ message: "userid wrong!" });
        }
        await userModel.findByIdAndUpdate({ _id: userid }, { Email: `delete${foundUser._id}${foundUser.Email}`, Delete: 1 }, { new: true });
        return res.send({ message: "User deleted successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const getUserDetails = async (req, res) => {
    let userid = req.body.userid;

    try {
        let result = await userModel.findById({ _id: userid });
        if (!result) {
            return res.send({ message: "userid wrong!" })
        }
        return res.send({ result, code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { getUsersList, updateStatusOfUser, deleteUserFromAdmin, getUserDetails }