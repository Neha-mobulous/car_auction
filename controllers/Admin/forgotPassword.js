
const adminModel = require("../../models/admin.model");
const { bcryptPass } = require("../../utils/bcrypt.password");


// ***************************Forgot Password API********************************
const forgotPassword = async (req, res) => {
    const { Email, newPassword } = req.body;

    if (!Email || !newPassword) {
        return res.status(400).send({ message: "Mail_id & newPassword are required!", code: 400 });
    }
    try {
        let hashPass = await bcryptPass(newPassword);
        let dbResult = await adminModel.findOneAndUpdate({ Email }, { Password: hashPass });
        if (!dbResult) {
            return res.status(400).send({ message: "Mail id not exist!", code: 400 });
        }
        return res.status(200).send({ message: "Password created successfully!", code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error });
    }
}

module.exports = { forgotPassword }