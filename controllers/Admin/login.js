const { loginSchema } = require("../../schemas/adminLoginSchema");
const adminModel = require("../../models/admin.model");
const { validatePassword } = require("../../utils/bcrypt.password");
const { createToken } = require("../../utils/verify.token");

const adminLogin = async (req, res) => {
    const { value, error } = loginSchema.validate(req.body);

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let result = await adminModel.findOne({ Email: value.Email });
        if (!result) {
            return res.status(401).send({ message: "Invalid credential", code: 401 });
        }
        let validatePass = await validatePassword(value.Password, result.Password);
        if (validatePass) {
            let token = await createToken(value);
            let details = await adminModel.findOneAndUpdate({ Email: value.Email }, { token: token }, { new: true });
            return res.status(200).send({ message: "Your Login successfully!", details, code: 200 });
        }
        return res.status(400).send({ message: "Your Password is wrong!", code: 400 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { adminLogin }

