const adminModel = require("../../models/admin.model");
const { generatedOTP } = require("../../utils/generated_otp");
const { sendOTP } = require("../../utils/nodemailer");

const sendOTPForForgotPassword = async (req, res) => {
    const Email = req.body.Email;

    if (!Email) {
        return res.status(400).send({ message: "Mail id is required!", code: 400 });
    }
    try {
        let otp = await generatedOTP();
        await sendOTP(Email, otp)
        let dateTime = new Date();
        let dbResult = await adminModel.findOneAndUpdate({ Email }, { OTP: otp, otpGeneratedAt: dateTime });
        if (!dbResult) {
            return res.status(400).send({ message: "Mail id wrong!", code: 400 });
        }
        return res.status(200).send({ message: "otp sent successfully!", OTP: otp, code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { sendOTPForForgotPassword };