const adminModel = require("../../models/admin.model");
const { bcryptPass, validatePassword } = require("../../utils/bcrypt.password");

const ResetPassword = async (req, res) => {
    const { Email, oldPassword, newPassword } = req.body;

    if (!Email || !newPassword || !oldPassword) {
        return res.status(400).send({ message: "Mail_id, oldPassword & newPassword are required!", code: 400 });
    }
    try {
        let hashPass = await bcryptPass(newPassword);
        let dbResult = await adminModel.findOne({ Email });
        if (!dbResult) {
            return res.status(400).send({ message: "Mail id not exist!", code: 400 });
        } else {
            let checkPass = await validatePassword(oldPassword, dbResult.Password);
            if (!checkPass) {
                return res.status(400).send({ message: "Old Password is incorrect!", code: 400 });
            }
            await adminModel.findOneAndUpdate({ Email }, { Password: hashPass });
            return res.status(200).send({ message: "New Password created successfully!", code: 200 });
        }
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { ResetPassword }