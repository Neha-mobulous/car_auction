const { loginSchema } = require("../../schemas/user.schema");
const userModel = require("../../models/user.model");
const { validatePassword } = require("../../utils/bcrypt.password");
const { createToken } = require("../../utils/verify.token");

const Login = async (req, res) => {
    const { value, error } = loginSchema.validate(req.body);

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let result = await userModel.findOne({ Email: value.Email });
        if (!result) {
            return res.status(400).send({ message: "Mail Not exist!", code: 400 });
        }
        let validatePass = await validatePassword(value.Password, result.Password);
        if (validatePass) {
            let token = await createToken(value);
            let details = await userModel.findOneAndUpdate({ Email: value.Email }, { token: token, Logout: false  }, { new: true });
            return res.status(200).send({ message: "Your Login successfully!", details, code: 200 });
        }
        return res.status(400).send({ message: "Your Password is wrong!", code: 400 })
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { Login }