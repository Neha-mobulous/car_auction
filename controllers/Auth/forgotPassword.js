const userModel = require("../../models/user.model");
const { generatedOTPForApp } = require("../../utils/generated_otp");
const { sendOTP } = require("../../utils/nodemailer");
const { bcryptPass } = require("../../utils/bcrypt.password");


// ********************SEND OTP FOR FORGOT PASSWORD*****************
const sendOTPForForgotPassword = async (req, res) => {
    const Email = req.body.Email;

    if (!Email) {
        return res.status(400).send({ message: "Mail id is required!", code: 400 })
    }
    try {
        let otp = await generatedOTPForApp();
        await sendOTP(Email, otp)
        let dateTime = new Date();
        let dbResult = await userModel.findOneAndUpdate({ Email }, { OTP: otp, otpGeneratedAt: dateTime });
        if (!dbResult) {
            return res.status(400).send({ message: "Mail id wrong!", code: 400 });
        }
        return res.status(200).send({ message: "otp sent successfully!", OTP: otp, code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}


// ***************************Forgot Password API********************************
const forgotPassword = async (req, res) => {
    const { Email, newPassword } = req.body;

    if (!Email || !newPassword) {
        return res.status(400).send({ message: "Mail_id & newPassword are required!", code: 400 });
    }
    try {
        let hashPass = await bcryptPass(newPassword);
        let dbResult = await userModel.findOneAndUpdate({ Email }, { Password: hashPass });
        if (!dbResult) {
            return res.status(400).send({ message: "Mail id not exist!", code: 400 });
        }
        return res.status(200).send({ message: "Password created successfully!", code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}



module.exports = { sendOTPForForgotPassword, forgotPassword }