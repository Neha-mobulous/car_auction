const userModel = require("../../models/user.model");
const { signupSchema } = require("../../schemas/user.schema");
const { bcryptPass } = require("../../utils/bcrypt.password");
const { generatedOTPForApp } = require("../../utils/generated_otp");
const { sendOTP } = require("../../utils/nodemailer");
const { createToken } = require("../../utils/verify.token");
const planHistoryModel = require("../../models/planHistory.model");
const paymentHistoryModel = require("../../models/paymentHistory.model");
const { planPurchaseSchema } = require("../../schemas/planPurchaseSchema");



const checkDuplicateMail = async (req, res) => {
    let Email = req.body.Email;
    if (!Email) {
        return res.status(400).send({ message: "Email is required!", code: 400 });
    }
    try {
        let result = await userModel.findOne({ Email: Email });
        if (result) {
            return res.status(409).send({ message: "This mail is already exist!", code: 409 });
        } else {
            return res.status(200).send({ message: "Mail Id Not exist!", code: 200 });
        }
    }
    catch (error) {
        return res.send({ Error: error, code: 500 })
    }
}

const sendOTPOnMail = async (req, res) => {
    const Email = req.body.Email;

    if (!Email) {
        return res.status(400).send({ message: "Email is required!", code: 400 });
    }
    try {
        let OTP = await generatedOTPForApp();
        await sendOTP(Email, OTP);
        return res.status(200).send({ message: "OTP Sent successfully!", OTP, code: 200 });

    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const Signup = async (req, res) => {
    const { value, error } = signupSchema.validate(req.body);

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let hashpass = await bcryptPass(value.Password);
        let token = await createToken(value);
        value.Password = hashpass;
        value.token = token;
        let user = await userModel.create(value);
        return res.status(200).send({ message: "Your account has been created successfully!", token, user, code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const planPurchase = async (req, res) => {
    let token = req.headers.authorization;
    const { value, error } = planPurchaseSchema.validate(req.body);

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let user = await userModel.findOne({ token });
        value.userType = user.userType;
        let data = { userId: user._id, planHistory: value };

        let planHis = await planHistoryModel.create(data);
        await userModel.findByIdAndUpdate({ _id: user._id }, { Plan: value });

        let paymentData = { userId: user._id, paymentId: planHis._id, paymentType: "plan", transactionId: "0", amount: "0" }
        // if (req.body.transcation.length > 0) {
        //     paymentData.transactionId = req.body.transcation;
        //     paymentData.amount = req.body.amount;
        // }
        if (!req.body.transactionId) {
            paymentData.transactionId = req.body.transcation;
            paymentData.amount = req.body.amount;
        }

        await paymentHistoryModel.create(paymentData);

        return res.status(200).send({ message: "plan history created successfully!", token, user, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { Signup, sendOTPOnMail, checkDuplicateMail, planPurchase }