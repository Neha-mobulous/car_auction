const planModel = require("../../models/plan.model");

const planList = async (req, res) => {
    let usertype = req.body.usertype;
    if (!usertype) {
        return res.send({ message: "usertype is required", code: 400 });
    }
    try {
        let planList = await planModel.find({ Delete: 0, Status: true, UserType: usertype }).limit(3);
        return res.send({ planList, code: 200 })
    }
    catch (error) {
        return res.send({ Error: error, code: 500 })
    }
}

module.exports = { planList }