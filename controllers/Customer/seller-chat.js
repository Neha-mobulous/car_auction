const auctionModel = require("../../models/auction.model");
const bidModel = require("../../models/bid.model");
const chatModel = require("../../models/chat.model");
const userModel = require("../../models/user.model");
const { ObjectId } = require('mongodb');
const { DateTime } = require("../../utils/date_time");
const moment = require("moment");

const sellerChatListing = async (req, res) => {
    let token = req.headers.authorization;
    let roomId = req.body.roomId;
    try {
        let user = await userModel.findOne({ token });
        let chatResult = await chatModel.find({ sender_id: user._id, roomId: roomId }, { _id: 0, __v: 0 });
        console.log(chatResult);
        return res.send({ result: chatResult, code: 200 })
    }
    catch (error) {
        return res.send({ Error: error, code: 500 })
    }
}


const getBidderOnCompleteAuction = async (req, res) => {
    let token = req.headers.authorization;
    let current_date_time = new Date();
    try {
        let currentDate = await DateTime(current_date_time);
        let user = await userModel.findOne({ token });
        let auctions = await auctionModel.aggregate([
            {
                $match: { SellerId: ObjectId(user._id) }
            },
            {
                $match: { Delete: 0 },
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'ProductId',
                    'foreignField': '_id',
                    'as': 'productDetail'
                }
            },
        ])
        let finalResult = [];
        for (auction of auctions) {
            const todayDate = moment(currentDate, 'DD-MM-YYYY').valueOf();
            const startDate = moment(auction.StartDate, 'DD-MM-YYYY').valueOf();
            if (startDate <= todayDate) {
                let bidders = await bidModel.aggregate([
                    { $match: { auctionid: auction._id } }
                ])
                var flag = 0;
                for (bid of bidders) {
                    if (bid.bidPrice < auction.Reserve_price) {
                        flag++;
                    }
                }
                if (flag == bidders.length) {
                    finalResult.push(auction);
                }
            }
        }
        return res.send({ auctions: finalResult, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error: error, code: 500 })
    }
}

const getBidderOnAuction = async (req, res) => {
    const auctionid = req.body.auctionid;
    if (!auctionid) {
        return res.send({ message: "auctionid is required!", code: 400 });
    }
    try {
        let bidders = await bidModel.aggregate([
            { $match: { auctionid: ObjectId(auctionid) } },
            {
                '$lookup': {
                    'from': 'users',
                    'localField': 'userid',
                    'foreignField': '_id',
                    'as': 'userDetail'
                }
            },
            {
                '$lookup': {
                    'from': 'auctions',
                    'localField': 'auctionid',
                    'foreignField': '_id',
                    'as': 'auctionDetail'
                }
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'auctionDetail.ProductId',
                    'foreignField': '_id',
                    'as': 'productDetail'
                }
            },
            { $unwind: { path: "$auctionDetail" } },
            { $unwind: { path: "$productDetail" } },
        ]);
        return res.send({ bidders: bidders, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error: error, code: 500 })
    }
}


module.exports = { sellerChatListing, getBidderOnCompleteAuction, getBidderOnAuction }