const bidModel = require("../../models/bid.model");
const userModel = require("../../models/user.model");
const auctionModel = require("../../models/auction.model");
const { ObjectId } = require('mongodb');
const { DateTime, CurrentTime, GetEndTime, GetEndDate } = require("../../utils/date_time");
const moment = require("moment");
const timezonemoment = require('moment-timezone');


const createBid = async (req, res) => {
    let token = req.headers.authorization;
    let auctionid = req.body.auctionid;
    let bidPrice = req.body.bidPrice;
    var roomId = Date.now();

    if (!auctionid || !bidPrice) {
        return res.send({ message: "auctionid or bidPrice is required!", code: 400 })
    }

    try {
        let user = await userModel.findOne({ token });
        let bid = await bidModel.findOne({ auctionid: auctionid, userid: user._id });
        if (!bid) {
            await bidModel.create({ auctionid: auctionid, userid: user._id, bidPrice: bidPrice, roomId: roomId });
        }
        else {
            await bidModel.findOneAndUpdate({ auctionid: auctionid, userid: user._id }, { bidPrice: bidPrice });
        }
        return res.send({ message: "bid create successfully!", code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error, error, code: 500 });
    }
}

const getBiddersListOnLiveAuction = async (req, res) => {
    let auctionid = req.body.auctionid;
    if (!auctionid) {
        return res.send({ message: "auctionid is required!", code: 400 })
    }

    try {
        let auctionDetail = await bidModel.aggregate([
            {
                $match: { auctionid: ObjectId(auctionid) }
            },
            {
                '$lookup': {
                    'from': 'users',
                    'localField': 'userid',
                    'foreignField': '_id',
                    'as': 'userDetail'
                }
            }
        ]);
        if (!auctionDetail) {
            return res.send({ message: "auction not exist!", code: 400 });
        }
        return res.send({ result: auctionDetail, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error, error, code: 500 });
    }
}


const getMyBidsList = async (req, res) => {
    let token = req.headers.authorization;
    let current_date_time = new Date();
    let currDate = await DateTime(current_date_time);
    let currTime = await CurrentTime(current_date_time);
    try {
        let user = await userModel.findOne({ token });
        let myBids = await bidModel.aggregate([
            { $match: { userid: ObjectId(user._id) } },
            {
                '$lookup': {
                    'from': 'auctions',
                    'localField': 'auctionid',
                    'foreignField': '_id',
                    'as': 'auctionDetail'
                }
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'auctionDetail.ProductId',
                    'foreignField': '_id',
                    'as': 'productDetail'
                }
            },
            { $unwind: { path: "$auctionDetail" } },
            { $unwind: { path: "$productDetail" } },
        ]);
        for (auction of myBids) {
            let getHighPrice = await bidModel.aggregate([
                { $match: { auctionid: ObjectId(auction._id) } },
                // { $match: { auctionid: auction._id } },
                { $sort: { bidPrice: -1 } },
                { $limit: 1 }
            ]);
            if (getHighPrice.length != 0) {
                auction.maxPrice = getHighPrice[0].bidPrice;
            } else {
                auction.maxPrice = '';
            }


            let EndTime = await GetEndTime(auction.auctionDetail.StartTime, auction.auctionDetail.Duration);
            let EndDate = await GetEndDate(auction.auctionDetail.StartDate, auction.auctionDetail.Duration);
            const todayDate = moment(currDate, 'DD-MM-YYYY').valueOf();
            const startDate = moment(auction.auctionDetail.StartDate, 'DD-MM-YYYY').valueOf();


            let dateTimeString = `${EndDate} ${EndTime}`;
            let startDateString = `${auction.auctionDetail.StartDate} ${auction.auctionDetail.StartTime}`;
            let startDateTimestamp = moment(startDateString, 'DD-MM-YYYY HH:mm:ss').valueOf();
            const endTimestamp = moment(dateTimeString, 'DD-MM-YYYY HH:mm:ss').valueOf();

            //************************************************************ */
            const currentTimezone = 'Asia/Kolkata';
            const currentDateTime = timezonemoment().tz(currentTimezone);
            const currentTime = currentDateTime.format('YYYY-MM-DD HH:mm:ss');
            const currentTimestamp = moment(currentTime).valueOf();

            // *****************************************************************


            if ((todayDate == startDate) && (currTime < auction.auctionDetail.StartTime)) {
                auction.auctionDetail.AuctionStatus = 'upcoming';
            }
            else if (todayDate < startDate) {
                auction.auctionDetail.AuctionStatus = 'upcoming';
            }
            else if ((currentTimestamp >= startDateTimestamp) && (currentTimestamp <= endTimestamp)) {
                auction.auctionDetail.AuctionStatus = 'inprogress';
            }

            let bidders = await bidModel.aggregate([
                { $match: { auctionid: auction.auctionid } },
                { $match: { bidPrice: { $gte: auction.bidPrice } } },
                { $match: { userid: { $ne: user._id } } }
            ]);

            if (currentTimestamp > endTimestamp) {
                if (bidders.length == 0) {
                    auction.auctionDetail.AuctionStatus = 'completed';
                    auction.MyBidStatus = 'Won';
                } else {
                    auction.auctionDetail.AuctionStatus = 'completed';
                    auction.MyBidStatus = 'Lost';
                }
            }
        }
        return res.send({ myBids, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error, error, code: 500 });
    }
}

module.exports = { createBid, getBiddersListOnLiveAuction, getMyBidsList }