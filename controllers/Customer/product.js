const subcategoryModel = require("../../models/subcategory.model");
const productModel = require("../../models/product.model");
const userModel = require("../../models/user.model");
const auctionModel = require("../../models/auction.model");
const { productSchema } = require("../../schemas/productSchema");
const { ObjectId } = require("mongodb");

const getAttributeOfSubcategory = async (req, res) => {
    let subcategoryid = req.body.subcategoryid;
    if (!subcategoryid) {
        return res.send({ message: "subcategory id required!", code: 400 })
    }
    try {
        let subcategoryAttribute = await subcategoryModel.find({ _id: subcategoryid }, { Attribute: 1 });
        return res.send({ subcategoryAttribute, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const createProductFromApp = async (req, res) => {
    let token = req.headers.authorization;
    const { value, error } = productSchema.validate(req.body);

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let findSellerId = await userModel.findOne({ token }, { _id: 1 });
        value.SellerId = findSellerId._id;
        await productModel.create(value);
        return res.send({ message: "Product created successfully.", code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error: error, code: 500 });
    }
}

const getSellerProductList = async (req, res) => {
    let token = req.headers.authorization;
    try {
        let findSellerId = await userModel.findOne({ token: token, Delete: 0 });
        let id = findSellerId._id.toString();
        let productResult = await productModel.aggregate([
            { $match: { SellerId: ObjectId(id) } },
            { $match: { Delete: 0 } },
            {
                $lookup: {
                    from: 'categories',
                    localField: 'CategoryId',
                    foreignField: '_id',
                    as: 'categoryDetail'
                }
            },
            {
                $lookup: {
                    from: 'subcategories',
                    localField: 'SubCategoryId',
                    foreignField: '_id',
                    as: 'subcategoryDetail'
                }
            },
            { $unwind: { path: "$categoryDetail" } },
            { $unwind: { path: "$subcategoryDetail" } },
        ])
        return res.send({ result: productResult, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error: error, code: 500 });
    }
}

const updateProductFromApp = async (req, res) => {
    const productid = req.body.productid;
    const value = req.body;
    if (!productid) {
        return res.send({ message: "product id required!", code: 400 });
    }
    try {
        let dbResult = await productModel.findByIdAndUpdate({ _id: productid }, value, { new: true });
        if (!dbResult) {
            return res.send({ message: "Product id not exist!", code: 400 });
        }
        return res.send({ message: "Product updated successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const deleteProductFromApp = async (req, res) => {
    const productid = req.body.productid;
    if (!productid) {
        return res.send({ message: "product id required!", code: 400 });
    }
    try {
        let dbResult = await productModel.findByIdAndUpdate({ _id: productid }, { Delete: 1 }, { new: true });
        if (!dbResult) {
            return res.send({ message: "product id not exist!", code: 400 });
        }
        return res.send({ message: "product deleted successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const getProductDetailFromApp = async (req, res) => {
    const productid = req.body.productid;

    if (!productid) {
        return res.send({ message: "product id required!", code: 400 });
    }

    try {
        let productDetail = await productModel.aggregate([
            {
                $match: { _id: ObjectId(productid) },
            },
            {
                '$lookup': {
                    'from': 'categories',
                    'localField': 'CategoryId',
                    'foreignField': '_id',
                    'as': 'category'
                }
            },
            {
                '$lookup': {
                    'from': 'subcategories',
                    'localField': 'SubCategoryId',
                    'foreignField': '_id',
                    'as': 'subcategory'
                }
            },

        ]);

        if (!productDetail) {
            return res.send({ message: "Product id not exist!", code: 400 });
        }
        return res.send({ result: productDetail, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { getAttributeOfSubcategory, createProductFromApp, getSellerProductList, updateProductFromApp, deleteProductFromApp, getProductDetailFromApp }