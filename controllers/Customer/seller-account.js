const accountModel = require("../../models/account.model");
const userModel = require("../../models/user.model");


const addAccount = async (req, res) => {
    const data = req.body;
    let token = req.headers.authorization;
    try {
        let user = await userModel.findOne({ token });
        data.userId = user._id;
        await accountModel.create(data);
        return res.send({ message: "Account added successfully!", code: 200 });

    }
    catch (error) {
        return res.send({ Error: error, code: 500 })
    }
}


const editAccount = async (req, res) => {
    const data = req.body;
    console.log(data);
    let token = req.headers.authorization;
    try {
        let user = await userModel.findOne({ token });
        await accountModel.findOneAndUpdate({ userId: user._id }, data);
        return res.send({ message: "Account edit successfully!", code: 200 });

    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 })
    }
}

const getAccount = async (req, res) => {
    let token = req.headers.authorization;
    try {
        let user = await userModel.findOne({ token });
        let result = await accountModel.findOne({ userId: user._id });
        return res.send({ result, code: 200 });

    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 })
    }
}

module.exports = { addAccount, editAccount, getAccount }