// const options = {
//     keyPrefix: 'student/',
//     bucket: "aitutor",
//     region: 'us-east-2',
//     accessKey: 'AKIA2AORTHGSFAJT4LXV',
//     secretKey: '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',
//     successActionStatus: 201
// };
const AWS_ID = 'AKIA2AORTHGSFAJT4LXV';
const AWS_SECRET = 'M635NZb2Z66dlhPOpINCjJk0bvY44c15TNZC4';
const AWS_BUCKET_NAME = 'aitutor';
const KEY = 'student/'
var AWS = require('aws-sdk');

const s3 = new AWS.S3({
    accessKeyId: AWS_ID,
    secretAccessKey: AWS_SECRET,
});

const deleteBucketImage = async (req, res) => {
    let keyName = req.body.keyName;
    if (!keyName) {
        return res.send({ message: 'keyName is required!', code: 400 });
    }
    try {
        const params = {
            Bucket: AWS_BUCKET_NAME,
            Key: `${KEY}${keyName}`
        };
        s3.deleteObject(params, (error, data) => {
            if (error) {
                console.log(error);
                return res.send({ Error: error, code: 500 });
            }
            return res.send({ message: 'File has been deleted successfully', code: 200 });
        });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { deleteBucketImage }