const auctionModel = require("../../models/auction.model");
const productModel = require("../../models/product.model");
const userModel = require("../../models/user.model");
const wishlistModel = require("../../models/wishlist.model");
const saveProductModel = require("../../models/saveproduct.model");
const { DateTime, CurrentTime, GetEndTime, GetEndDate } = require("../../utils/date_time");
const bidModel = require("../../models/bid.model");
const moment = require("moment");
const timezonemoment = require('moment-timezone');


const getAuctionList = async (req, res) => {
    const token = req.headers.authorization;
    let value = req.body;
    let current_date_time = new Date();
    let currDate = await DateTime(current_date_time);
    let currTime = await CurrentTime(current_date_time);
    if (!value.longitude || !value.latitude) {
        return res.send({ message: "longitude or latitude required!", code: 400 })
    }
    try {
        let AuctionProducts = await auctionModel.aggregate([
            {
                $geoNear: {
                    near: { type: "Point", coordinates: [Number(value.longitude), Number(value.latitude)] },
                    spherical: true,
                    minDistance: 0,
                    maxDistance: 400000,
                    distanceField: 'distance'
                }
            },
            {
                $match: { Delete: 0 },
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'ProductId',
                    'foreignField': '_id',
                    'as': 'productDetail'
                }
            },
            {
                '$lookup': {
                    'from': 'categories',
                    'localField': 'CategoryId',
                    'foreignField': '_id',
                    'as': 'categoryDetail'
                }
            },
            {
                '$lookup': {
                    'from': 'subcategories',
                    'localField': 'SubCategoryId',
                    'foreignField': '_id',
                    'as': 'subcategoryDetail'
                }
            },
            { $unwind: { path: '$productDetail' } },
            { $unwind: { path: '$categoryDetail' } },
            { $unwind: { path: '$subcategoryDetail' } },

        ]);

        let user = await userModel.findOne({ token });
        let newArray = [];
        for (auction of AuctionProducts) {
            if (!((auction.SellerId).toString() == (user._id).toString())) {
                let bidResult = await bidModel.findOne({ userid: user._id, auctionid: auction._id });
                if (!bidResult) {
                    let EndTime = await GetEndTime(auction.StartTime, auction.Duration);
                    let resultFinal = {}
                    resultFinal._id = auction._id;
                    resultFinal.AuctionId = auction.AuctionId;
                    resultFinal.Location = auction.Location;
                    resultFinal.Place = auction.Place;
                    resultFinal.City = auction.City;
                    resultFinal.Model = auction.Model;
                    resultFinal.Quantity = auction.Quantity;
                    resultFinal.Lot_description = auction.Lot_description;
                    resultFinal.StartDate = auction.StartDate;
                    resultFinal.StartTime = auction.StartTime;
                    resultFinal.Duration = auction.Duration;
                    resultFinal.Bid_start_rate = auction.Bid_start_rate;
                    resultFinal.Reserve_price = auction.Reserve_price;
                    resultFinal.AuctionType = auction.AuctionType;
                    resultFinal.ProductDetail = auction.productDetail
                    resultFinal.categoryDetail = auction.categoryDetail
                    resultFinal.subcategoryDetail = auction.subcategoryDetail
                    let getWishlist = await wishlistModel.findOne({ userid: user._id, productid: auction.productDetail._id });
                    if (getWishlist) {
                        if (getWishlist.status) {
                            resultFinal.wishlist = true;
                        } else {
                            resultFinal.wishlist = false;
                        }
                    } else {
                        resultFinal.wishlist = false;
                    }
                    let getSaveSearch = await saveProductModel.findOne({ userid: user._id, productid: auction.productDetail._id });
                    if (getSaveSearch) {
                        if (getSaveSearch.status) {
                            resultFinal.saveSearch = true;
                        } else {
                            resultFinal.saveSearch = false;
                        }
                    } else {
                        resultFinal.saveSearch = false;
                    }
                    let EndDate = await GetEndDate(auction.StartDate, auction.Duration);
                    const currentDate = moment(currDate, 'DD-MM-YYYY').valueOf();
                    const startDate = moment(auction.StartDate, 'DD-MM-YYYY').valueOf();

                    let dateTimeString = `${EndDate} ${EndTime}`;
                    let startDateString = `${auction.StartDate} ${auction.StartTime}`;
                    let startDateTimestamp = moment(startDateString, 'DD-MM-YYYY HH:mm:ss').valueOf();
                    const endTimestamp = moment(dateTimeString, 'DD-MM-YYYY HH:mm:ss').valueOf();

                    //************************************************************ */
                    const currentTimezone = 'Asia/Kolkata';
                    const currentDateTime = timezonemoment().tz(currentTimezone);
                    const currentTime = currentDateTime.format('YYYY-MM-DD HH:mm:ss');
                    const currentTimestamp = moment(currentTime).valueOf();

                    // *****************************************************************

                    if ((startDate == currentDate) && (auction.StartTime > currTime)) {
                        resultFinal.AuctionStatus = 'upcoming';
                        newArray.push(resultFinal);
                    }
                    else if (currentDate < startDate) {
                        resultFinal.AuctionStatus = 'upcoming';
                        newArray.push(resultFinal);
                    }
                    else if ((currentTimestamp >= startDateTimestamp) && (currentTimestamp <= endTimestamp)) {
                        resultFinal.AuctionStatus = 'inprogress';
                        newArray.push(resultFinal);
                    }
                }
            }
        }
        return res.send({ result: newArray, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error: error, code: 500 });
    }
}


const getBidderProductList = async (req, res) => {
    const token = req.headers.authorization;
    let value = req.body;
    if (!value.longitude || !value.latitude) {
        return res.send({ message: "longitude or latitude required!", code: 400 });
    }
    try {
        let user = await userModel.findOne({ token });
        let Products = await productModel.aggregate([
            {
                $geoNear: {
                    near: { type: "Point", coordinates: [Number(value.longitude), Number(value.latitude)] },
                    spherical: true,
                    minDistance: 0,
                    maxDistance: 400000,
                    distanceField: 'distance'
                }
            },
            {
                $match: { Delete: 0 },
            },
            {
                $match: { ProductType: "for sale" },
            },

        ]);
        let newArray = [];
        for (product of Products) {
            let resultFinal = {};
            let getWishlist = await wishlistModel.findOne({ userid: user._id, productid: product._id });
            if (getWishlist) {
                if (getWishlist.status) {
                    resultFinal.wishlist = true;
                } else {
                    resultFinal.wishlist = false;
                }
            } else {
                resultFinal.wishlist = false;
            }
            resultFinal.ProductDetail = product;
            newArray.push(resultFinal);
        }

        for (product of Products) {
            let resultFinal = {};
            let getSaveSearchProduct = await saveProductModel.findOne({ userid: user._id, productid: product._id });
            if (getSaveSearchProduct) {
                if (getSaveSearchProduct.status) {
                    resultFinal.saveSearch = true;
                } else {
                    resultFinal.saveSearch = false;
                }
            } else {
                resultFinal.saveSearch = false;
            }
            resultFinal.ProductDetail = product;
            newArray.push(resultFinal);
        }
        return res.send({ result: newArray, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error: error, code: 500 });
    }
}

const getProductListFromAllOverCountry = async (req, res) => {
    try {
        let products = await productModel.find({ Delete: 0, ProductType: "for sale" });
        console.log(products);
        return res.send({ result: products, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { getAuctionList, getBidderProductList, getProductListFromAllOverCountry }