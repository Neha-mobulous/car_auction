const saveProductModel = require("../../models/saveproduct.model");
const userModel = require("../../models/user.model");

const saveProduct = async (req, res) => {
    let token = req.headers.authorization;
    let productid = req.body.productid;
    let status = req.body.status;

    if (!productid || status == undefined) {
        return res.send({ message: "productid or status required!", code: 400 })
    }

    try {
        let userDetail = await userModel.findOne({ token });
        let saveProductResult = await saveProductModel.findOneAndUpdate({ userid: userDetail._id, productid: productid }, { status: status });
        if (!saveProductResult) {
            await saveProductModel.create({
                userid: userDetail._id,
                productid: productid
            });
        }
        return res.send({ message: "save product successfully!", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}


const saveProductListing = async (req, res) => {
    let token = req.headers.authorization;
    try {
        let userDetail = await userModel.findOne({ token });
        let productResult = await saveProductModel.aggregate([
            { $match: { status: true } },
            { $match: { userid: userDetail._id } },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'productid',
                    'foreignField': '_id',
                    'as': 'ProductDetail'
                }
            },
        ])

        let newArray = [];
        for (product of productResult) {
            let obj = {}
            obj._id = product._id;
            obj.productid = product.productid;
            obj.userid = product.userid;
            obj.ProductDetail = product.ProductDetail[0];
            newArray.push(obj)
        }
        return res.send({ result: newArray, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

module.exports = { saveProduct, saveProductListing }