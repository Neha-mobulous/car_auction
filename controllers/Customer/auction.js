const categoryModel = require("../../models/category.model");
const subcategoryModel = require("../../models/subcategory.model");
const productModel = require("../../models/product.model");
const auctionModel = require("../../models/auction.model");
const { auctionSchema } = require("../../schemas/auctionSchema");
const userModel = require("../../models/user.model");
const { ObjectId } = require('mongodb');
const { DateTime, CurrentTime, GetEndTime, GetEndDate } = require("../../utils/date_time");
const bidModel = require("../../models/bid.model");
const saveProductModel = require("../../models/saveproduct.model");
const wishlistModel = require("../../models/wishlist.model");
const moment = require("moment");
const timezonemoment = require('moment-timezone');

const getlistCategory = async (req, res) => {
    try {
        let categoryList = await categoryModel.find({ Delete: 0 });
        return res.send({ categoryList, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const getlistSubCategory = async (req, res) => {
    let categoryid = req.body.categoryid;
    if (!categoryid) {
        return res.send({ message: "category id required!", code: 400 })
    }
    try {
        let subcategoryList = await subcategoryModel.find({ CategoryId: categoryid, Delete: 0 });
        let result = [];
        for (sub of subcategoryList) {
            let newobj = {}
            newobj._id = sub._id;
            newobj.SubCategoryName = sub.SubCategoryName;
            result.push(newobj)
        }
        return res.send({ result, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}


const getlistProduct = async (req, res) => {
    let token = req.headers.authorization;
    let subcategoryid = req.body.subcategoryid;
    if (!subcategoryid) {
        return res.send({ message: "subcategory id required!", code: 400 })
    }
    try {
        let user = await userModel.findOne({ token });
        let auctions = await auctionModel.find({ SellerId: user._id });
        let productList = await productModel.find({
            SubCategoryId: subcategoryid, Delete: 0, ProductType: 'for auction', AdminVerify: 'approved',
            $or: [
                { SellerId: { $eq: user._id } },
                { CreatedBy: { $eq: 'admin' } }
            ]
        });
        let auctionProducts = [];
        for (let auction of auctions) {
            auctionProducts.push((auction.ProductId).toString());
        }
        let resultArray = [];
        for (product of productList) {
            let id = (product._id).toString();
            if (!auctionProducts.includes(id)) {
                resultArray.push(product);
            }
        }
        return res.send({ productList: resultArray, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}


const createAuction = async (req, res) => {
    const randomNumber = Math.floor((Math.random() * 100000000) + 1);
    const AuctionId = randomNumber;
    const { value, error } = auctionSchema.validate(req.body);
    const token = req.headers.authorization;
    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let sellerDetail = await userModel.findOne({ token: token });
        value.AuctionId = AuctionId;
        value.SellerId = sellerDetail._id.toString();
        await auctionModel.create(value);
        return res.send({ message: "Auction created successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error, error, code: 500 });
    }
}


const getAuctionListBySeller = async (req, res) => {
    let SellerId = req.body.SellerId;
    if (!SellerId) {
        return res.send({ message: "SellerId required!", code: 400 });
    }
    try {
        let auctionResult = await auctionModel.aggregate([
            {
                $match: { SellerId: ObjectId(SellerId) },
            },
            { '$match': { "Delete": 0 } },
            {
                '$lookup': {
                    'from': 'users',
                    'localField': 'SellerId',
                    'foreignField': '_id',
                    'as': 'userDetails'
                }
            },
        ])
        return res.send({ auctionResult, total: auctionResult.length, code: 200 })

    } catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const GetInprogressAuction = async (req, res) => {
    let token = req.headers.authorization;
    let current_date_time = new Date();
    try {
        let startDate = await DateTime(current_date_time);
        let user = await userModel.findOne({ token });
        let result = await auctionModel.aggregate([
            {
                $match: { SellerId: ObjectId(user._id) }
            },
            {
                $match: { StartDate: startDate },
            },
            {
                $match: { Delete: 0 },
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'ProductId',
                    'foreignField': '_id',
                    'as': 'product'
                }
            },
        ])
        let newArray = [];
        for (value of result) {
            let EndTime = await GetEndTime(value.StartTime, value.Duration);
            let EndDate = await GetEndDate(value.StartDate, value.Duration);

            let dateTimeString = `${EndDate} ${EndTime}`;
            let startDateString = `${value.StartDate} ${value.StartTime}`;
            let startDateTimestamp = moment(startDateString, 'DD-MM-YYYY HH:mm:ss').valueOf();
            const endTimestamp = moment(dateTimeString, 'DD-MM-YYYY HH:mm:ss').valueOf();

            //*****************************************************************
            const currentTimezone = 'Asia/Kolkata';
            const currentDateTime = timezonemoment().tz(currentTimezone);
            const currentTime = currentDateTime.format('YYYY-MM-DD HH:mm:ss');
            const currentTimestamp = moment(currentTime).valueOf();

            // *****************************************************************

            if ((currentTimestamp >= startDateTimestamp) && (currentTimestamp <= endTimestamp)) {
                let getHighPrice = await bidModel.aggregate([
                    { $match: { auctionid: ObjectId(value._id) } },
                    { $sort: { bidPrice: -1 } },
                    { $limit: 1 }
                ]);
                let resultFinal = {}
                resultFinal._id = value._id;
                resultFinal.AuctionId = value.AuctionId;
                resultFinal.Location = value.Location;
                resultFinal.Place = value.Place;
                resultFinal.City = value.City;
                resultFinal.Model = value.Model;
                resultFinal.Quantity = value.Quantity;
                resultFinal.Lot_description = value.Lot_description;
                resultFinal.StartDate = value.StartDate;
                resultFinal.StartTime = value.StartTime;
                resultFinal.Duration = value.Duration;
                resultFinal.Bid_start_rate = value.Bid_start_rate;
                resultFinal.Reserve_price = value.Reserve_price;
                resultFinal.AuctionType = value.AuctionType;
                resultFinal.AuctionStatus = 'inprogress';
                if (getHighPrice.length != 0) {
                    resultFinal.currentHighPrice = getHighPrice[0].bidPrice;
                } else {
                    resultFinal.currentHighPrice = '';
                }
                resultFinal.ProductDetail = value.product[0]
                newArray.push(resultFinal);
            }
        }
        return res.send({ result: newArray, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error, error, code: 500 });
    }
}

const GetInprogressAuctionDetail = async (req, res) => {
    const auctionid = req.body.auctionid;
    if (!auctionid) {
        return res.send({ message: "auction id required!", code: 400 });
    }
    try {
        let auctionResult = await auctionModel.aggregate([
            {
                $match: { _id: ObjectId(auctionid) },
            },
            {
                $match: { AuctionStatus: "inprogress" },
            },
            {
                $match: { Delete: 0 },
            },
        ])
        if (!auctionResult) {
            return res.send({ message: "auction id not exist!", code: 400 });
        }
        return res.send({ result: auctionResult, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error, error, code: 500 });
    }
}


const GetUpcomingAuction = async (req, res) => {
    let token = req.headers.authorization;
    let current_date_time = new Date();
    try {
        let currDate = await DateTime(current_date_time);
        let currTime = await CurrentTime(current_date_time);
        let user = await userModel.findOne({ token });
        let auctionResult = await auctionModel.aggregate([
            {
                $match: { SellerId: ObjectId(user._id) }
            },
            {
                $match: { Delete: 0 },
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'ProductId',
                    'foreignField': '_id',
                    'as': 'product'
                }
            },
        ])
        let newArray = [];
        for (auction of auctionResult) {
            let resultFinal = {}
            resultFinal._id = auction._id;
            resultFinal.AuctionId = auction.AuctionId;
            resultFinal.Location = auction.Location;
            resultFinal.Place = auction.Place;
            resultFinal.City = auction.City;
            resultFinal.Model = auction.Model;
            resultFinal.Quantity = auction.Quantity;
            resultFinal.Lot_description = auction.Lot_description;
            resultFinal.StartDate = auction.StartDate;
            resultFinal.StartTime = auction.StartTime;
            resultFinal.Duration = auction.Duration;
            resultFinal.Bid_start_rate = auction.Bid_start_rate;
            resultFinal.Reserve_price = auction.Reserve_price;
            resultFinal.AuctionType = auction.AuctionType;
            resultFinal.ProductDetail = auction.product[0]

            const todayDate = moment(currDate, 'DD-MM-YYYY').valueOf();
            const startDate = moment(auction.StartDate, 'DD-MM-YYYY').valueOf();

            if ((startDate == todayDate) && (auction.StartTime > currTime)) {
                resultFinal.AuctionStatus = 'upcoming';
                newArray.push(resultFinal);
            }
            else if (todayDate < startDate) {
                resultFinal.AuctionStatus = 'upcoming';
                newArray.push(resultFinal);
            } else {
                newArray = [];
            }
        }
        console.log(newArray);
        return res.send({ result: newArray, total: newArray.length, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error, error, code: 500 });
    }
}


const GetUpcomingAuctionDetail = async (req, res) => {
    const auctionid = req.body.auctionid;
    if (!auctionid) {
        return res.send({ message: "auction id required!", code: 400 });
    }
    try {

        let auctionResult = await auctionModel.aggregate([
            {
                $match: { _id: ObjectId(auctionid) },
            },
            {
                $match: { AuctionStatus: "upcoming" },
            },
            {
                $match: { Delete: 0 },
            },
        ])
        if (!auctionResult) {
            return res.send({ message: "auction id not exist!", code: 400 });
        }
        return res.send({ result: auctionResult, code: 200 });
    }
    catch (error) {
        return res.send({ Error, error, code: 500 });
    }
}

const DeleteUpcomingAuction = async (req, res) => {
    const auctionid = req.body.auctionid;
    if (!auctionid) {
        return res.send({ message: "auction id required!", code: 400 });
    }
    try {
        let result = await auctionModel.findOneAndUpdate({ _id: auctionid, AuctionStatus: "upcoming" }, { Delete: 1 });
        if (!result) {
            return res.send({ message: "auction id not exist!", code: 400 });
        }
        return res.send({ message: "Upcoming auction deleted!", code: 200 });
    }
    catch (error) {
        return res.send({ Error, error, code: 500 });
    }
}


const GetPasteventAuction = async (req, res) => {
    let token = req.headers.authorization;
    try {
        let user = await userModel.findOne({ token });

        let result = await auctionModel.aggregate([
            {
                $match: { SellerId: ObjectId(user._id) }
            },
            {
                $match: { Delete: 0 },
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'ProductId',
                    'foreignField': '_id',
                    'as': 'product'
                }
            },
        ])

        let newArray = [];
        for (value of result) {
            let getHighPrice = await bidModel.aggregate([
                { $match: { auctionid: ObjectId(value._id) } },
                { $sort: { bidPrice: -1 } },
                { $limit: 1 }
            ]);
            let resultFinal = {}
            resultFinal._id = value._id;
            resultFinal.AuctionId = value.AuctionId;
            resultFinal.Location = value.Location;
            resultFinal.Place = value.Place;
            resultFinal.Model = value.Model;
            resultFinal.Quantity = value.Quantity;
            resultFinal.Lot_description = value.Lot_description;
            resultFinal.StartDate = value.StartDate;
            resultFinal.StartTime = value.StartTime;
            resultFinal.Duration = value.Duration;
            resultFinal.Bid_start_rate = value.Bid_start_rate;
            resultFinal.Reserve_price = value.Reserve_price;
            resultFinal.AuctionType = value.AuctionType;
            resultFinal.ProductDetail = value.product[0]
            if (getHighPrice.length != 0) {
                resultFinal.currentHighPrice = getHighPrice[0].bidPrice;
            } else {
                resultFinal.currentHighPrice = '';
            }

            let EndTime = await GetEndTime(value.StartTime, value.Duration);
            let EndDate = await GetEndDate(value.StartDate, value.Duration);

            let dateTimeString = `${EndDate} ${EndTime}`;
            const endTimestamp = moment(dateTimeString, 'DD-MM-YYYY HH:mm:ss').valueOf();

            //************************************************************ */
            const currentTimezone = 'Asia/Kolkata';
            const currentDateTime = timezonemoment().tz(currentTimezone);
            const currentTime = currentDateTime.format('YYYY-MM-DD HH:mm:ss');
            const currentTimestamp = moment(currentTime).valueOf();

            // *****************************************************************

            if (currentTimestamp > endTimestamp) {
                resultFinal.AuctionStatus = 'completed';
                newArray.push(resultFinal);
            }
        }
        return res.send({ result: newArray, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error, error, code: 500 });
    }
}

const getAuctionByCity = async (req, res) => {
    const token = req.headers.authorization;
    const City = req.body.City;
    let current_date_time = new Date();
    let currDate = await DateTime(current_date_time);
    let currTime = await CurrentTime(current_date_time)
    if (!City) {
        return res.send({ message: "City is required!", code: 400 });
    } try {
        let auctions = await auctionModel.aggregate([
            {
                $match: { Delete: 0 },
            },
            {
                $match: { City: City },
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'ProductId',
                    'foreignField': '_id',
                    'as': 'product'
                }
            },
        ])
        let user = await userModel.findOne({ token });
        let newArray = []
        console.log(auctions, "******************auctions****************");
        for (auction of auctions) {

            if (!((auction.SellerId).toString() == (user._id).toString())) {
                let resultFinal = {}
                resultFinal.AuctionId = auction.AuctionId;
                resultFinal.Location = auction.Location;
                resultFinal.Place = auction.Place;
                resultFinal.City = auction.City;
                resultFinal.Model = auction.Model;
                resultFinal.Quantity = auction.Quantity;
                resultFinal.Lot_description = auction.Lot_description;
                resultFinal.StartDate = auction.StartDate;
                resultFinal.StartTime = auction.StartTime;
                resultFinal.Duration = auction.Duration;
                resultFinal.Bid_start_rate = auction.Bid_start_rate;
                resultFinal.Reserve_price = auction.Reserve_price;
                resultFinal.AuctionType = auction.AuctionType;
                resultFinal.ProductDetail = auction.product[0];


                let getWishlist = await wishlistModel.findOne({ userid: user._id, productid: auction.product[0]._id });
                if (getWishlist) {
                    if (getWishlist.status) {
                        resultFinal.wishlist = true;
                    } else {
                        resultFinal.wishlist = false;
                    }
                } else {
                    resultFinal.wishlist = false;
                }
                let getSaveSearch = await saveProductModel.findOne({ userid: user._id, productid: auction.product[0]._id });
                if (getSaveSearch) {
                    if (getSaveSearch.status) {
                        resultFinal.saveSearch = true;
                    } else {
                        resultFinal.saveSearch = false;
                    }
                } else {
                    resultFinal.saveSearch = false;
                }
                newArray.push(resultFinal);
            }
        }
        console.log(newArray, "*******************newArray******************");
        return res.send({ result: newArray, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}


const getAuctionFromCountry = async (req, res) => {
    let current_date_time = new Date();
    let currDate = await DateTime(current_date_time);
    try {
        let TodayAuctions = await auctionModel.aggregate([
            {
                $match: { Delete: 0 },
            },
            {
                $match: { StartDate: currDate }
            },
            {
                $group: { _id: '$City', totalAuctions: { $sum: 1 } }
            }
        ]);
        let UpcomingAuctions = await auctionModel.aggregate([
            {
                $match: { Delete: 0 },
            },
            {
                $match: {
                    StartDate: {
                        "$lt": currDate
                    }
                }
            },
            {
                $group: { _id: '$City', totalAuctions: { $sum: 1 } }
            }
        ])
        return res.send({ result: { TodayAuctions, UpcomingAuctions }, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { getlistCategory, getlistSubCategory, getlistProduct, createAuction, getAuctionListBySeller, GetInprogressAuction, GetInprogressAuctionDetail, GetUpcomingAuction, GetUpcomingAuctionDetail, DeleteUpcomingAuction, GetPasteventAuction, getAuctionByCity, getAuctionFromCountry }