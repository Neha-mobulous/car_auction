const wishlistModel = require("../../models/wishlist.model");
const userModel = require("../../models/user.model");
const productModel = require("../../models/product.model");


const CreateWishlist = async (req, res) => {
    let token = req.headers.authorization;
    let productid = req.body.productid;
    let status = req.body.status;

    if (!productid || status == undefined) {
        return res.send({ message: "productid or status required!", code: 400 })
    }
    try {
        let userDetail = await userModel.findOne({ token });
        let wishlistResult = await wishlistModel.findOneAndUpdate({ userid: userDetail._id, productid: productid }, { status: status });
        if (!wishlistResult) {
            await wishlistModel.create({
                userid: userDetail._id,
                productid: productid
            });
        }
        return res.send({ message: "wishlist status update successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 })
    }
}

const wishlistListing = async (req, res) => {
    let token = req.headers.authorization;
    try {
        let user = await userModel.findOne({ token: token });
        let id = user._id;
        let productResult = await wishlistModel.aggregate([
            { $match: { status: true } },
            { $match: { userid: id } },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'productid',
                    'foreignField': '_id',
                    'as': 'ProductDetail'
                }
            },
            {
                '$lookup': {
                    'from': 'auctions',
                    'localField': 'productid',
                    'foreignField': 'ProductId',
                    'as': 'AuctionDetail'
                }
            },
        ])
        let newArray = [];
        for (product of productResult) {
            let obj = {}
            obj._id = product._id;
            obj.productid = product.productid;
            obj.userid = product.userid;
            obj.ProductDetail = product.ProductDetail[0];
            if (product.AuctionDetail.length > 0) {
                obj.AuctionDetail = product.AuctionDetail[0];
            }
            newArray.push(obj)
        }
        return res.send({ result: newArray, code: 200 });
    }
    catch (error) {
        console.log(error);
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { CreateWishlist, wishlistListing }