const chatModel = require("../../models/chat.model");
const userModel = require("../../models/user.model");
const chatRoomModel = require("../../models/chat-room.model");
const bidModel = require("../../models/bid.model");
const { ObjectId } = require('mongodb');

// All seller  chat list 
const chatList = async (req, res) => {
    let token = req.headers.authorization;
    try {
        let user = await userModel.findOne({ token })
        let getMsg = await chatRoomModel.aggregate([
            { $match: { receiverId: ObjectId(user._id) } },
            {
                '$lookup': {
                    'from': 'users',
                    'localField': 'senderId',
                    'foreignField': '_id',
                    'as': 'userDetails'
                }
            },
            {
                '$lookup': {
                    'from': 'auctions',
                    'localField': 'auctionId',
                    'foreignField': '_id',
                    'as': 'auctionDetail'
                }
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'auctionDetail.ProductId',
                    'foreignField': '_id',
                    'as': 'productDetail'
                }
            },
            { $unwind: { path: "$auctionDetail" } },
            { $unwind: { path: "$productDetail" } },
        ]);
        let sellerList = [];
        for (seller of getMsg) {
            let getBidders = await bidModel.aggregate([{ $match: { auctionid: ObjectId(seller.auctionId) } }]);
            seller.bidders = getBidders;
            sellerList.push(seller)
        }
        return res.send({ result: sellerList });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}


// Bidder chat hostory list
const bidderChatListing = async (req, res) => {
    let token = req.headers.authorization;
    let roomId = req.body.roomId;
    if (!roomId) {
        return res.send({ message: "roomId is required!", code: 200 })
    }
    try {
        let user = await userModel.findOne({ token });
        let chatResult = await chatModel.find({ senderId: user._id, roomId: roomId });
        return res.send({ result: chatResult, code: 200 })
    }
    catch (error) {
        return res.send({ Error: error, code: 500 })
    }
}

module.exports = { bidderChatListing, chatList }