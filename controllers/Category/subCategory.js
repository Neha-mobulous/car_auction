const subcategoryModel = require("../../models/subcategory.model");
const { subCategorySchema } = require("../../schemas/subCategorySchema");
const adminModel = require("../../models/admin.model");


const createSubCategory = async (req, res) => {
    const token = req.headers.authorization;
    const { value, error } = subCategorySchema.validate(req.body)
    if (!token) {
        return res.status(400).send({ message: "token are required!", code: 400 });
    }

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }

        await subcategoryModel.create(value);
        return res.send({ message: "Sub Category created successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 })
    }
}

const GetSubCategoryList = async (req, res) => {
    const token = req.headers.authorization;
    const page = parseInt(req.body.page) || 1;
    const limit = parseInt(req.body.limit) || 10;
    const skip = (page - 1) * limit;
    const searchText = req.body.searchText || "";
    if (!token) {
        return res.status(400).send({ message: "token are required!", code: 400 });
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        let result = await subcategoryModel.aggregate([
            { '$match': { "Delete": 0 } },
            {
                '$lookup': {
                    'from': 'categories',
                    'localField': 'CategoryId',
                    'foreignField': '_id',
                    'as': 'category'
                }
            }, {
                '$lookup': {
                    'from': 'products',
                    'localField': '_id',
                    'foreignField': 'SubCategoryId',
                    'as': 'productInfo'
                }
            },
            {
                '$match': {
                    '$or': [
                        { "category.CategoryName": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                        { "SubCategoryName": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                    ]
                }
            },
            { '$skip': skip },
            { '$limit': limit },
            { '$sort': { createdAt: -1 } }
        ]);
        return res.send({ result, total: result.length, code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const GetSubCategoryByCategoryId = async (req, res) => {
    const categoryid = req.body.categoryid;
    try {
        let result = await subcategoryModel.find({ CategoryId: categoryid, Delete: 0 }).sort({ createdAt: -1 });
        if (!result) {
            return res.send({ message: "Sub Category not exist!", code: 400 });
        }
        return res.send({ result, code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const GetSubCategoryById = async (req, res) => {
    const token = req.headers.authorization;
    const subcategoryid = req.body.subcategoryid;
    if (!token) {
        return res.status(400).send({ message: "token are required!", code: 400 });
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        let result = await subcategoryModel.findOne({ _id: subcategoryid, Delete: 0 }).sort({ createdAt: -1 });
        if (!result) {
            return res.send({ message: "Sub Category not exist!", code: 400 });
        }
        return res.send({ result, code: 200 });
    }
    catch (error) {
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const updateSubCategoryById = async (req, res) => {
    const token = req.headers.authorization;
    const subcategoryid = req.body.subcategoryid;
    const value = req.body;
    if (!token) {
        return res.status(400).send({ message: "token are required!", code: 400 });
    }
    if (!subcategoryid) {
        return res.send({ message: "sub category id required!", code: 400 });
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        let dbResult = await subcategoryModel.findByIdAndUpdate({ _id: subcategoryid }, value, { new: true });
        if (!dbResult) {
            return res.send({ message: "Sub Category id not exist!", code: 400 });
        }
        return res.send({ message: "Sub Category updated successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const deleteSubCategoryById = async (req, res) => {
    const token = req.headers.authorization;
    const subcategoryid = req.body.subcategoryid;
    if (!token) {
        return res.status(400).send({ message: "token are required!", code: 400 });
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        let dbResult = await subcategoryModel.findByIdAndUpdate({ _id: subcategoryid }, { Delete: 1 }, { new: true });
        if (!dbResult) {
            return res.send({ message: "Sub Category id not exist!", code: 400 });
        }
        return res.send({ message: "Sub Category deleted successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const updateStatusOfSubCategory = async (req, res) => {
    const status = req.body.status;
    const subcategoryid = req.body.subcategoryid;
    const token = req.headers.authorization;
    if (!token || !subcategoryid) {
        return res.status(400).send({ message: "token or subcategoryid are required!", code: 400 });
    }
    if (status == undefined) {
        return res.send({ message: "status is required!", code: 400 })
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        let dbResult = await subcategoryModel.findByIdAndUpdate({ _id: subcategoryid }, { Status: status });
        if (!dbResult) {
            return res.send({ message: "SubCategory id not exist!", code: 400 });
        }
        return res.send({ message: "SubCategory status has changed successfully!", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { createSubCategory, GetSubCategoryList, GetSubCategoryById, updateSubCategoryById, deleteSubCategoryById, updateStatusOfSubCategory, GetSubCategoryByCategoryId }