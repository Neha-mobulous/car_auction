const productModel = require("../../models/product.model");
const subcategoryModel = require("../../models/subcategory.model");
const { productSchema } = require("../../schemas/productSchema");
const { ObjectId } = require('mongodb');




const getAttributeBabeOnSubcategory = async (req, res) => {
    let subcategoryid = req.body.subcategoryid;
    if (!subcategoryid) {
        return res.send({ message: "subcategory id required!", code: 400 })
    }
    try {
        let subcategoryAttribute = await subcategoryModel.find({ _id: subcategoryid }, { Attribute: 1 });
        return res.send({ subcategoryAttribute, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}



const createProdcut = async (req, res) => {
    const { value, error } = productSchema.validate(req.body)

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }
    try {
        value.AdminVerify = "approved";
        value.CreatedBy = "admin"
        await productModel.create(value)
        return res.send({ message: "Product created successfully.", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 })
    }
}



const getListProdcut = async (req, res) => {
    const page = parseInt(req.body.page) || 1;
    const limit = parseInt(req.body.limit) || 10;
    const skip = (page - 1) * limit;
    const searchText = req.body.searchText || "";

    try {
        let result = await productModel.aggregate([
            { '$match': { "Delete": 0 } },
            { "$sort": { createdAt: -1 } },
            { '$skip': skip },
            { '$limit': limit },
            {
                '$lookup': {
                    'from': 'categories',
                    'localField': 'CategoryId',
                    'foreignField': '_id',
                    'as': 'category'
                }
            }, {
                '$lookup': {
                    'from': 'subcategories',
                    'localField': 'SubCategoryId',
                    'foreignField': '_id',
                    'as': 'subcategory'
                }
            },
            {
                '$match': {
                    '$or': [
                        { "ProductName": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                        { "category.CategoryName": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                        { "subcategory.SubCategoryName": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                    ]
                }
            },

        ])
        let count = await productModel.countDocuments({ ...searchText, Delete: 0 });
        return res.send({ result, total: count, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}

const updateProduct = async (req, res) => {
    const productid = req.body.productid;
    const value = req.body;
    if (!productid) {
        return res.send({ message: "product id required!", code: 400 });
    }
    try {
        let dbResult = await productModel.findByIdAndUpdate({ _id: productid }, value, { new: true });
        if (!dbResult) {
            return res.send({ message: "Product id not exist!", code: 400 });
        }
        return res.send({ message: "Product updated successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}


const deleteProduct = async (req, res) => {
    const productid = req.body.productid;
    if (!productid) {
        return res.send({ message: "product id required!", code: 400 });
    }
    try {
        let dbResult = await productModel.findByIdAndUpdate({ _id: productid }, { Delete: 1 }, { new: true });
        if (!dbResult) {
            return res.send({ message: "product id not exist!", code: 400 });
        }
        return res.send({ message: "product deleted successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const updateProductStatus = async (req, res) => {
    const status = req.body.status;
    const productid = req.body.productid;
    if (status == undefined) {
        return res.send({ message: "status is requried!", code: 400 })
    }
    try {
        let dbResult = await productModel.findByIdAndUpdate({ _id: productid }, { Status: status });
        if (!dbResult) {
            return res.send({ message: "Product id not exist!", code: 400 });
        }
        return res.send({ message: "Product status has changed successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const getProductDetail = async (req, res) => {
    const productid = req.body.productid;

    if (!productid) {
        return res.send({ message: "product id required!", code: 400 });
    }

    try {
        let productDetail = await productModel.aggregate([
            {
                $match: { _id: ObjectId(productid) },
            },
            {
                '$lookup': {
                    'from': 'categories',
                    'localField': 'CategoryId',
                    'foreignField': '_id',
                    'as': 'category'
                }
            },
            {
                '$lookup': {
                    'from': 'subcategories',
                    'localField': 'SubCategoryId',
                    'foreignField': '_id',
                    'as': 'subcategory'
                }
            },

        ]);

        if (!productDetail) {
            return res.send({ message: "Product id not exist!", code: 400 });
        }
        return res.send({ result: productDetail, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}

const verifyProduct = async (req, res) => {
    const verifystatus = req.body.verifystatus;
    const productid = req.body.productid;
    if (!productid || verifystatus == undefined) {
        return res.send({ message: "product id and verifystatus required!", code: 400 });
    }
    try {
        let dbResult = await productModel.findByIdAndUpdate({ _id: productid }, { AdminVerify: verifystatus }, { new: true });
        if (!dbResult) {
            return res.send({ message: "Product id not exist!", code: 400 });
        }
        return res.send({ message: "Product updated successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

module.exports = { createProdcut, getListProdcut, updateProduct, deleteProduct, updateProductStatus, getAttributeBabeOnSubcategory, getProductDetail, verifyProduct }