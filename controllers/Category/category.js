const categoryModel = require("../../models/category.model");
const adminModel = require("../../models/admin.model");
const { categorySchema } = require("../../schemas/categorySchema");

const createCategory = async (req, res) => {
    const token = req.headers.authorization;
    const { value, error } = categorySchema.validate(req.body);

    if (error) {
        return res.status(400).send({ Error: error.details[0].message, code: 400 });
    }

    if (!token) {
        return res.status(400).send({ message: "token are required!", code: 400 });
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        await categoryModel.create(value);
        return res.send({ message: "Category created successfully.", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}



const getAllCategories = async (req, res) => {
    try {
        let allCategories = await categoryModel.find({ Delete: 0 });
        return res.send({ result: allCategories, code: 200 });

    } catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}


const getlistCategory = async (req, res) => {
    const page = parseInt(req.body.page) || 1;
    const limit = parseInt(req.body.limit) || 10;
    const skip = (page - 1) * limit;
    const token = req.headers.authorization;
    const searchText = req.body.searchText || "";
    if (!token) {
        return res.status(400).send({ message: "token are required!", code: 400 });
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        let result = await categoryModel.aggregate([
            { '$match': { "Delete": 0 } },
            {
                '$lookup': {
                    'from': 'subcategories',
                    'localField': '_id',
                    'foreignField': 'CategoryId',
                    'as': 'SubCategories'
                }
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': '_id',
                    'foreignField': 'CategoryId',
                    'as': 'ProductList'
                }
            },
            {
                '$match': {
                    '$or': [
                        { "CategoryName": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                        { "SubCategories.SubCategoryName": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                        { "ProductList.ProductName": { $regex: '.*' + searchText + '.*', $options: 'i' } },
                    ]
                }
            },
            { '$skip': skip },
            { '$limit': limit },
            { '$sort': { createdAt: -1 } }
        ]);

        let FilnalResult = [];
        for (cat of result) {
            let oneObject = {}
            oneObject._id = cat._id
            oneObject.CategoryName = cat.CategoryName;
            oneObject.NumberOfSubCategories = cat.SubCategories.length;
            oneObject.NumberOfProducts = cat.ProductList.length;
            oneObject.createdAt = cat.createdAt;
            oneObject.Status = cat.Status;
            FilnalResult.push(oneObject);
        }
        return res.send({ FilnalResult, total: result.length, code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.status(500).send({ Error: error, code: 500 });
    }
}


const updateCategory = async (req, res) => {
    const categoryid = req.body.categoryid;
    const value = req.body;
    const token = req.headers.authorization;
    if (!token) {
        return res.status(400).send({ message: "token are required!", code: 400 });
    }
    if (!categoryid) {
        return res.send({ message: "category id required!", code: 400 });
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        let dbResult = await categoryModel.findByIdAndUpdate({ _id: categoryid }, value, { new: true });
        if (!dbResult) {
            return res.send({ message: "Category id not exist!", code: 400 });
        }
        return res.send({ message: "Category updated successfully!", code: 200 });
    }
    catch (error) {
        return res.send({ Error: error, code: 500 });
    }
}

const deleteCategory = async (req, res) => {
    const token = req.headers.authorization;
    const categoryid = req.body.categoryid;
    if (!token || !categoryid) {
        return res.status(400).send({ message: "token or categoryid are required!", code: 400 });
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        let dbResult = await categoryModel.findByIdAndUpdate({ _id: categoryid }, { Delete: 1 }, { new: true });
        if (!dbResult) {
            return res.send({ message: "Category id not exist!", code: 400 });
        }
        return res.send({ message: "Category deleted successfully!", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}

const actionCategory = async (req, res) => {
    const status = req.body.status;
    const categoryid = req.body.categoryid;
    const token = req.headers.authorization;
    if (!token || !categoryid) {
        return res.status(400).send({ message: "token are required!", code: 400 });
    }
    if (status == undefined) {
        return res.send({ message: "status is required!", code: 400 })
    }
    try {
        let adminResult = await adminModel.findOne({ token: token });
        if (!adminResult) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
        let dbResult = await categoryModel.findByIdAndUpdate({ _id: categoryid }, { Status: status });
        if (!dbResult) {
            return res.send({ message: "Category id not exist!", code: 400 });
        }
        return res.send({ message: "Category status has changed successfully!", code: 200 });
    }
    catch (error) {
        console.log(error)
        return res.send({ Error: error, code: 500 });
    }
}


module.exports = { createCategory, getAllCategories, getlistCategory, updateCategory, deleteCategory, actionCategory }