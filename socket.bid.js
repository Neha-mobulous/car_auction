const bidModel = require("./models/bid.model");
const { ObjectId } = require('mongodb');

const lastBidder = async (data) => {
    try {
        let bidder = await bidModel.aggregate([
            { $match: { auctionid: ObjectId(data.auctionId) } },
            {
                '$lookup': {
                    'from': 'users',
                    'localField': 'userid',
                    'foreignField': '_id',
                    'as': 'userDetail'
                }
            },
            {
                '$lookup': {
                    'from': 'auctions',
                    'localField': 'auctionid',
                    'foreignField': '_id',
                    'as': 'auctionDetail'
                }
            },
            {
                '$lookup': {
                    'from': 'products',
                    'localField': 'auctionDetail.ProductId',
                    'foreignField': '_id',
                    'as': 'productDetail'
                }
            },
            { $unwind: { path: "$auctionDetail" } },
            { $unwind: { path: "$productDetail" } },
            // { $sort: { bidPrice: -1 } },
            // { $limit: 1 },
        ]);
        return bidder;
    }
    catch (error) {
        return error;
    }
}


const currentAllBidders = async (data) => {
    try {
        let allBidders = await bidModel.aggregate([
            {
                $match: { auctionid: ObjectId(data.auctionId) }
            },
            {
                '$lookup': {
                    'from': 'users',
                    'localField': 'userid',
                    'foreignField': '_id',
                    'as': 'userDetail'
                }
            }
        ]);

        return allBidders;
    }
    catch (error) {
        return error;
    }
}

module.exports = { lastBidder, currentAllBidders };