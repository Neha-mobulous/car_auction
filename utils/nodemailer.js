const nodemailer = require("nodemailer")
const SERVICE = "gmail";
const USER_MAIL = "neha.mobulous@gmail.com";
const PASSWORD = "etzdnwkgnppyhvkq";

const transporter = nodemailer.createTransport({
    service: SERVICE,
    auth: {
        user: USER_MAIL,
        pass: PASSWORD
    }
});

const sendOTP = async (Email, OTP) => {
    var mailOptions = {
        from: USER_MAIL,
        to: Email,
        subject: 'Verification OTP',
        text: `${OTP}`
    };
    return await transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return error;
        } else {
            return info.response;
        }
    });
}

module.exports = { sendOTP }