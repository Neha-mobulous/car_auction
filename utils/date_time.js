let nineDate = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
let nineMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9]
const moment = require("moment");


function DateTime(date) {

    let getdate = date.getDate()
    let getmonth = date.getMonth() + 1;
    let getyear = date.getFullYear()
    if (nineDate.includes(getdate)) {
        getdate = `0${getdate}`
    }
    if (nineMonth.includes(getmonth)) {
        getmonth = `0${getmonth}`
    }
    let fullDate = `${getdate}-${getmonth}-${getyear}`;
    return fullDate;

}

const CurrentTime = async (current_date) => {
    let a = current_date.toLocaleTimeString("en-US", { hour12: false });
    let currentTime = a.split(" ")[0]
    let currTime = moment(currentTime, "HH:mm:ss").add('5:30', 'hours').format('HH:mm:ss');
    return currTime;
}

const GetEndTime = async (startTime, duration) => {
    let day = Math.floor(Number(duration) / 1440);
    let minutes = (Number(duration) % 1440);
    let endTime = moment(startTime, "HH:mm:ss").add(day, 'days').add(minutes, 'minutes').format('HH:mm:ss');
    return endTime;
}

const GetEndDate = async (startDate, duration) => {
    const convertedDate = moment(startDate, 'DD-MM-YYYY').toDate();
    var newDate = new Date(convertedDate.getTime() + (duration * 60 * 1000));
    let enddate = await DateTime(newDate)
    return enddate;
}

module.exports = { DateTime, CurrentTime, GetEndTime, GetEndDate }