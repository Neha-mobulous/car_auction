const bcrypt = require('bcryptjs');

const bcryptPass = async (password) => {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    return hashedPassword;
};

const validatePassword = async (password, hash) => {
    return await bcrypt
        .compare(password, hash)
        .then(res => {
            return res;
        })
        .catch(err => { return err.message })
}

module.exports = { bcryptPass, validatePassword }