function generatedOTP() {
    let OTP = Math.floor(100000 + Math.random() * 900000);
    return OTP;
}


function generatedOTPForApp() {
    let OTP = Math.floor(1000 + Math.random() * 9000);
    return OTP;
}

module.exports = { generatedOTP, generatedOTPForApp }