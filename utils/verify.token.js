const SECRET_KEY = "neha_1234_yadav";
const jwt = require("jsonwebtoken");
const adminModel = require("../models/admin.model");
const userModel = require("../models/user.model");

const createToken = async (data) => {
    const token = await jwt.sign(data, SECRET_KEY);
    return token;
}

const AdminAuth = async (req, res, next) => {
    const token =
        req.body.authorization || req.query.authorization || req.headers["authorization"];

    if (!token) {
        return res.status(403).send({ message: "A token is required for authentication", code: 403 });
    }
    try {
        let verifyToken = await adminModel.findOne({ token });
        if (verifyToken == null) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
    } catch (err) {
        return res.status(401).send({ message: "Invalid Token", coe: 401 });
    }
    return next();
}

const UserAuth = async (req, res, next) => {
    const token =
        req.body.token || req.query.token || req.headers["authorization"];

    if (!token) {
        return res.status(403).send({ message: "A token is required for authentication", code: 403 });
    }
    try {
        let verifyToken = await userModel.findOne({ token });
        if (verifyToken == null) {
            return res.send({ message: "Not Authorized!", code: 401 });
        }
    } catch (err) {
        return res.status(401).send({ message: "Invalid Token", coe: 401 });
    }
    return next();
};

module.exports = { createToken, AdminAuth, UserAuth }