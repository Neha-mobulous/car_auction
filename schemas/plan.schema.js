const Joi = require("joi");

const PlanSchema = Joi.object({
    UserType: Joi.string(),
    PlanName: Joi.string().required(),
    Plan_price: Joi.string().required(),
    Plan_icon: Joi.string(),
    ValidTill: Joi.string().required(),
    Number_Auction: Joi.number(),
    description: Joi.string(),
    UserType: Joi.string(),
    Access: Joi.array()
});

module.exports = { PlanSchema }