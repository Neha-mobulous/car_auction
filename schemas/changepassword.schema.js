const Joi = require("joi");


const changePasswordSchema = Joi.object({
    CurrentPassword: Joi.string().required(),
    NewPassword: Joi.string().required()
})

module.exports = { changePasswordSchema }