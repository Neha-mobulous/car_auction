const Joi = require("joi");

const auctionSchema = Joi.object({
    CategoryId: Joi.string().required(),
    SubCategoryId: Joi.string().required(),
    ProductId: Joi.string().required(),
    Location: Joi.object().required(),
    Place: Joi.string().required(),
    City: Joi.string().required(),
    Country: Joi.string().required(),
    Model: Joi.string().required(),
    Quantity: Joi.string().required(),
    Lot_description: Joi.string().required(),
    StartDate: Joi.string().required(),
    StartTime: Joi.string().required(),
    Duration: Joi.string().required(),
    Bid_start_rate: Joi.string(),
    Reserve_price: Joi.string(),
    AuctionType: Joi.string()
});


module.exports = { auctionSchema }