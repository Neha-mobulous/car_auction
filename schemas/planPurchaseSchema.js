
const Joi = require("joi");

const planPurchaseSchema = Joi.object({
    PlanName: Joi.string(),
    Plan_price: Joi.string(),
    paymentType: Joi.string(),
    ValidTill: Joi.string(),
    Number_Auction: Joi.number(),
    description: Joi.string(),
    planStatus: Joi.string(),
    UserType: Joi.string(),
    Access: Joi.array()
});

module.exports = { planPurchaseSchema }