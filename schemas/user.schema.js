const Joi = require("joi")

const signupSchema = Joi.object({
    Fullname: Joi.string().required(),
    Email: Joi.string().required(),
    Mobile: Joi.string(),
    Password: Joi.string(),
});

const loginSchema = Joi.object({
    Email: Joi.string().required(),
    Password: Joi.string().required()
});

module.exports = { signupSchema, loginSchema };