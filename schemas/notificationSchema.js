const Joi = require("joi");


const NotificationSchema = Joi.object({
    MyAccount: Joi.boolean().required(),
    LiveAuction: Joi.boolean().required(),
    MyBids: Joi.boolean().required(),
    MyRecomendation: Joi.boolean().required()
})

module.exports = { NotificationSchema }