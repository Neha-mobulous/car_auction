const Joi = require("joi");


const addDebitCredit = Joi.object({
    CardNumber: Joi.string().required(),
    ExpiresEnd: Joi.string().required(),
    CVV: Joi.string().required()
})

module.exports = { addDebitCredit }