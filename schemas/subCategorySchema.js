const Joi = require("joi");

const subCategorySchema = Joi.object({
    SubCategoryName: Joi.string().required(),
    CategoryId: Joi.string().required(),
    Attribute: Joi.array()
})


module.exports = { subCategorySchema }