const Joi = require("joi");

const subAdminSchema = Joi.object({
    Name: Joi.string().required(),
    Email: Joi.string().required(),
    Role: Joi.string(),
    SubAdminRole: Joi.string().required(),
    Mobile: Joi.string(),
    Password: Joi.string().required(),
    Access: Joi.array()
});

module.exports = { subAdminSchema }