const Joi = require("joi");

const loginSchema = Joi.object({
    Email: Joi.string().required(),
    Password: Joi.string().required()
});


module.exports = { loginSchema }