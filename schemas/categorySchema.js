const Joi = require("joi");

const categorySchema = Joi.object({
    CategoryName: Joi.string().required()
});


module.exports = { categorySchema }