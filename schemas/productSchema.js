const Joi = require("joi");

const productSchema = Joi.object({
    ProductName: Joi.string().required(),
    ProductType: Joi.string().valid('for sale', 'for auction').required(),
    Model: Joi.string(),
    Attribute: Joi.array(),
    CategoryId: Joi.string().required(),
    SubCategoryId: Joi.string().required(),
    BuyerCommission: Joi.string(),
    SellerCommission: Joi.string(),
    Location: Joi.object(),
    Place: Joi.string(),
    City: Joi.string(),
    Country: Joi.string(),
    State: Joi.string(),
    description: Joi.string(),
    Photos: Joi.array()
});


module.exports = { productSchema }