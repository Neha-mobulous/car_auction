const Joi = require("joi");


const contactUs = Joi.object({
    Name: Joi.string().required(),
    Email: Joi.string().required(),
    Subject: Joi.string().required(),
    Message: Joi.string().required()
})

module.exports = { contactUs }