const chatModel = require("./models/chat.model");
const chatRoomModel = require("./models/chat-room.model");

const socketIo = async (data) => {
    try {
        await chatModel.create({ senderId: data.senderId, receiverId: data.receiverId, message: data.message, chatId: data.chatId, roomId: data.roomId });
        let getChat = await chatRoomModel.find({ roomId: data.roomId });
        if (getChat.length == 0) {
            console.log(data.auctionId, "**************auction id  from socket*******");
            await chatRoomModel.create({ senderId: data.senderId, receiverId: data.receiverId, lastmessage: data.message, roomId: data.roomId, auctionId: data.auctionId });
        } else {
            await chatRoomModel.findOneAndUpdate({ roomId: data.roomId }, { lastmessage: data.message });
        }
    }
    catch (error) {
        console.log(error)
        return error;
    }
}

module.exports = socketIo 