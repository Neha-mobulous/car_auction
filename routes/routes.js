const router = require('express').Router();
const { AdminAuth, UserAuth } = require("../utils/verify.token");


// ****************************************AUTH CONTROLLERS********************
const userSignupController = require("../controllers/Auth/signup");
const userLoginController = require("../controllers/Auth/login");
const profileController = require("../controllers/Profile/MyAccount");
const forgotPasswordController = require("../controllers/Auth/forgotPassword");
const resetPasswordController = require("../controllers/Auth/resetPassword");
const getNotificationController = require("../controllers/Profile/getNotification");


// *********************************PROFILE CONTROLLERS*************************
const paymentMethodConroller = require("../controllers/Profile/PaymentMethod");
const adddebitcreditcardController = require("../controllers/Profile/debitCreditCard");
const logoutController = require("../controllers/Profile/logout");
const changePasswordController = require("../controllers/Profile/changePassword");
const notificationController = require("../controllers/Profile/notification");
const changeLanguageController = require("../controllers/Profile/changeLanguage");
const contactUsController = require("../controllers/Profile/contactUs");
const updateUserTypeController = require("../controllers/Profile/updateUser");
const updateUserProfileController = require("../controllers/Profile/updateUserProfile");


// *******************************ADMIN CONTROLLERS**********************************
const adminLoginController = require("../controllers/Admin/login");
const adminLogoutController = require("../controllers/Admin/logout");
const sendOTPForForgotPassword = require("../controllers/Admin/sendOTP");
const FotgotPasswordControler = require("../controllers/Admin/forgotPassword");
const ResetPasswordController = require("../controllers/Admin/restetPassword");
const SubAdminController = require("../controllers/Admin/SubAdmin");
const sellerVerifyController = require("../controllers/Admin/sellerVerify");
const adminUserController = require("../controllers/Admin/users");
const adminDashboardController = require("../controllers/Admin/dashboard");
const adminAuctionController = require("../controllers/Admin/auction");


// *************************************SELLER CONTROLLER*****************************
const uploadDocumentsController = require("../controllers/Seller/upload.document");
const createSellerController = require("../controllers/Seller/checkSeller");
const bidController = require("../controllers/Seller/bid");

// ********************************ADMIN CATEGORY CONTROLLER*************************************
const CategoryController = require("../controllers/Category/category");
const SubCategoryController = require("../controllers/Category/subCategory");
const ProductController = require("../controllers/Category/product");

//*********************************ADMIN PLAN CONTROLLER ***********************************
const adminPlanController = require("../controllers/Admin/plan");


// *********************************CUSTOMER AUCTION CONTROLLER****************************
const CustomerAuctionController = require("../controllers/Customer/auction");

// ***********************************CUSTOMER PRODUCT CONTROLLER**************************
const CustomerProductController = require("../controllers/Customer/product");

// ********************************CUSTOMER BIDDER CONTROLLER ******************************
const CustomerBidderController = require("../controllers/Customer/bidder");

// *******************************CONTOMER SAVEPRODUCT CONTROLLER **************************
const CustomerSaveProductController = require("../controllers/Customer/saveproduct");

// ***********************************CUSTOMER PLAN CONTROLLER******************************
const CustomerPlanController = require("../controllers/Customer/plan");

// *************************************CUSTOMER WISHLIST CONTROLLER ************************
const CustomerWishlistController = require("../controllers/Customer/wishlist");

// **********************************CUTOMER BIDS CONTROLLER ********************************
const CutomerBidController = require("../controllers/Customer/bid");

// ***********************************CUSTOMER CHATING CONTROLLER****************************
const CustomerChatingController = require("../controllers/Customer/seller-chat");
const CustomerBidderChatController = require("../controllers/Customer/bidder-chat");



// **************************************S3 BUCKET CONTROLLER********************************
const CutomerBucketController = require("../controllers/Customer/s3.bucket");



// **************************************ACCOUNT CONTROLLER ********************************
const AccountController = require("../controllers/Customer/seller-account");

// *************************************PAYMENT CONTROLLER**********************************
const PaymentController = require("../controllers/Profile/payment");


// **************************AUTH APIS**************************************
router.post("/checkduplicatemail", userSignupController.checkDuplicateMail);
router.post("/sendotp", userSignupController.sendOTPOnMail);
router.post("/signup", userSignupController.Signup);
router.post("/login", userLoginController.Login);
router.post("/auth/sendotp", forgotPasswordController.sendOTPForForgotPassword)
router.post("/auth/forgotpassword", forgotPasswordController.forgotPassword);
router.post("/auth/resetpassword", resetPasswordController.resetPassword);



// ******************************MY PROFILE APIS***********************************
router.get("/getdetails", UserAuth, profileController.MyAccount);
router.post("/addpaymentmethod", UserAuth, paymentMethodConroller.AddPaymentMethod);
router.post("/adddebitcreditcard", UserAuth, adddebitcreditcardController.AddDebitCreditCard);
router.get("/logout", UserAuth, logoutController.Logout);
router.post("/changepassword", UserAuth, changePasswordController.changePassword);
router.put("/notificationchange", UserAuth, notificationController.Notification);
router.post("/changelanguage", UserAuth, changeLanguageController.changeLanguage);
router.post("/contactus", UserAuth, contactUsController.contactWithAdmin);
router.post("/updateusertype", UserAuth, updateUserTypeController.UpdateUserType);
router.get("/getnotification", UserAuth, getNotificationController.getNotifications);
router.post("/updateuserprofile", UserAuth, updateUserProfileController.updateUserProfile);


// ************************************ADMIN APIS************************************
router.post("/adminlogin", adminLoginController.adminLogin);
router.post("/adminlogout", AdminAuth, adminLogoutController.adminLogout);
router.post("/admin/sendotp", AdminAuth, sendOTPForForgotPassword.sendOTPForForgotPassword);
router.post("/admin/forgotpassword", FotgotPasswordControler.forgotPassword);
router.post("/admin/resetpassword", AdminAuth, ResetPasswordController.ResetPassword);
router.post("/createsubadmin", AdminAuth, SubAdminController.CreateSubAdmin);
router.post("/subadminaccessdetails", AdminAuth, SubAdminController.SubAdminRoleAccess);
router.post("/getsubadmin", AdminAuth, SubAdminController.getSubAdmin);
router.put("/deletesubadmin", AdminAuth, SubAdminController.DeleteSubAdmin);
router.put("/updatesubadminstatus", AdminAuth, SubAdminController.updatSubAdminStatus);
router.put("/updatesubadmin", AdminAuth, SubAdminController.EditSubAdmin);
router.post("/sellerverify", AdminAuth, sellerVerifyController.sellerVerify);
router.post("/getuserslist", AdminAuth, adminUserController.getUsersList);
router.put("/updatestatusofuser", AdminAuth, adminUserController.updateStatusOfUser);
router.put("/deleteuserfromadmin", AdminAuth, adminUserController.deleteUserFromAdmin);
router.post("/getuserdetailfromadmin", AdminAuth, adminUserController.getUserDetails);
router.post("/getdashboarddetails", adminDashboardController.Dashboard);
router.get("/create-internet-retrieve-fee", AdminAuth, adminDashboardController.createInternetAndRetrieval);
router.post("/update-internet-retrieve-fee", AdminAuth, adminDashboardController.updateInternetAndRetrieval);
router.get("/get-internet-retrieve-fee", adminDashboardController.getInternetAndRetrieval);


// ****************************************ADMIN AUCTION APIs************************************
router.post("/getauctionlist", AdminAuth, adminAuctionController.GetAuctionList);
router.put("/deleteauctionfromadmin", AdminAuth, adminAuctionController.DeleteAuctionFromAdmin);
router.post("/getdetailauctionfromadmin", AdminAuth, adminAuctionController.AuctionDetailFromAdmin);
router.post("/verifyauction", AdminAuth, adminAuctionController.verifyAuction);

// **************************************SELLER API**********************************
router.post("/uploaddocuments", uploadDocumentsController.uploadDocuments);
router.get("/createseller", createSellerController.verifySellerFromAdmin);
router.post("/getbidsdetails", bidController.getBidDetailsForSeller);


// *******************************CATEGORY MANAGEMENT APIs****************************************
router.post("/createcategory", CategoryController.createCategory);
router.get("/getallcategories", AdminAuth, CategoryController.getAllCategories);
router.post("/getcategories", CategoryController.getlistCategory);
router.post("/updatecategory", CategoryController.updateCategory);
router.put("/deletecategory", CategoryController.deleteCategory);
router.put("/actionstatus", CategoryController.actionCategory);
router.post("/createsubcategory", SubCategoryController.createSubCategory);
router.post("/getsubcategorieslist", SubCategoryController.GetSubCategoryList);
router.post("/getsubcategorybyid", SubCategoryController.GetSubCategoryById);
router.post("/getsubcategorybycateogeryid", AdminAuth, SubCategoryController.GetSubCategoryByCategoryId);
router.put("/updatesubcategory", SubCategoryController.updateSubCategoryById);
router.put("/deletesubcategory", SubCategoryController.deleteSubCategoryById);
router.put("/updatestatus", SubCategoryController.updateStatusOfSubCategory);
router.post("/getattributebaseonsubcategory", AdminAuth, ProductController.getAttributeBabeOnSubcategory);
router.post("/createproduct", AdminAuth, ProductController.createProdcut);
router.post("/getlistproducts", AdminAuth, ProductController.getListProdcut);
router.put("/updateproduct", AdminAuth, ProductController.updateProduct);
router.put("/deleteproduct", AdminAuth, ProductController.deleteProduct);
router.put("/updatestatusproduct", AdminAuth, ProductController.updateProductStatus);
router.post("/getproductdetail", AdminAuth, ProductController.getProductDetail);
router.post("/verifyproduct", AdminAuth, ProductController.verifyProduct);


// ********************************CUSTOMER AUCTION APIs********************************
router.get("/getlistcategory", UserAuth, CustomerAuctionController.getlistCategory);
router.post("/getlistsubcategory", UserAuth, CustomerAuctionController.getlistSubCategory);
router.post("/getlistproduct", UserAuth, CustomerAuctionController.getlistProduct);
router.post("/createauction", UserAuth, CustomerAuctionController.createAuction);
router.post("/getauctionlistbysellet", UserAuth, CustomerAuctionController.getAuctionListBySeller);
router.get("/getinprogressauction", UserAuth, CustomerAuctionController.GetInprogressAuction);
router.post("/getinprogressauctiondetail", UserAuth, CustomerAuctionController.GetInprogressAuctionDetail);
router.get("/getupcomingauction", UserAuth, CustomerAuctionController.GetUpcomingAuction);
router.post("/getupcomingauctiondetail", UserAuth, CustomerAuctionController.GetUpcomingAuctionDetail);
router.post("/deleteupcomingauction", UserAuth, CustomerAuctionController.DeleteUpcomingAuction);
router.get("/getpasteventauction", UserAuth, CustomerAuctionController.GetPasteventAuction);
router.post("/getauctionbylocation", UserAuth, CustomerAuctionController.getAuctionByCity);
router.get("/getcountryauctions", CustomerAuctionController.getAuctionFromCountry);

// ********************************CUSTOMER PRODUCT APIs************************************
router.post("/getsubcategoryattribute", UserAuth, CustomerProductController.getAttributeOfSubcategory);
router.post("/createproductfromapp", UserAuth, CustomerProductController.createProductFromApp);
router.get("/getproductlist", UserAuth, CustomerProductController.getSellerProductList);
router.put("/updateproductfromapp", UserAuth, CustomerProductController.updateProductFromApp);
router.put("/deleteproductfromapp", UserAuth, CustomerProductController.deleteProductFromApp);
router.post("/getdetailproductfromapp", UserAuth, CustomerProductController.getProductDetailFromApp);


// ***********************************CUSTOMER BIDDER API******************************** 
router.post("/getauctionproducts", UserAuth, CustomerBidderController.getAuctionList);
router.post("/get-bidder-product-list", UserAuth, CustomerBidderController.getBidderProductList);
router.get("/get-products-all-over-country", UserAuth, CustomerBidderController.getProductListFromAllOverCountry);


// ******************************* CUSTOMER SAVE PRODUCT API******************************
router.post("/saveproduct", UserAuth, CustomerSaveProductController.saveProduct);
router.get("/saveproductlist", UserAuth, CustomerSaveProductController.saveProductListing);


// *********************************ADMIN PLAN API***************************************
router.post("/createfeature", AdminAuth, adminPlanController.createFeature);
router.get("/getfeatures", AdminAuth, adminPlanController.getFeatureOfPlan);
router.post("/createplan", AdminAuth, adminPlanController.createPlan);
router.post("/getplanlist", AdminAuth, adminPlanController.PlanListing);
router.put("/editplan", AdminAuth, adminPlanController.updatePlan);
router.put("/updateplanstatus", AdminAuth, adminPlanController.updatePlanStatus);
router.put("/deleteplan", AdminAuth, adminPlanController.deletePlan);


// *********************************CUSTOMER PLAN API **********************************
router.post("/getplanlistapp", CustomerPlanController.planList);

// ******************************CUSTOMER WISHLIST API**********************************
router.post("/createwishlist", UserAuth, CustomerWishlistController.CreateWishlist);
router.get("/getwishlist", UserAuth, CustomerWishlistController.wishlistListing);


// ********************************CUSTOMER BIDS API**********************************
router.post("/createmybid", UserAuth, CutomerBidController.createBid);
router.post("/getbidslist", UserAuth, CutomerBidController.getBiddersListOnLiveAuction);
router.get("/get-my-bids-list", UserAuth, CutomerBidController.getMyBidsList);


// ***********************************CUSTOMER SELLER CHAT APIs*******************************
router.post("/chatlisting", UserAuth, CustomerChatingController.sellerChatListing);
router.get("/get-bidders-complete-auction", UserAuth, CustomerChatingController.getBidderOnCompleteAuction);
router.post("/get-bidders-on-auction", UserAuth, CustomerChatingController.getBidderOnAuction);


// *************************************BIDDER CHAT API*********************************
router.post("/get-bidder-chat-list", UserAuth, CustomerBidderChatController.bidderChatListing);
router.get("/get-chat", UserAuth, CustomerBidderChatController.chatList);

// **********************************S3 BUCKET API**************************************
router.post('/delete-bucket-image', UserAuth, CutomerBucketController.deleteBucketImage);


// ************************************ACCOUNT APIs*************************************
router.post("/add-account", UserAuth, AccountController.addAccount);
router.post("/edit-accound", UserAuth, AccountController.editAccount);
router.get("/get-account", UserAuth, AccountController.getAccount);



// ***********************************PAYMENT APIs**************************************
router.post("/add-plan", UserAuth, userSignupController.planPurchase);
router.get("/get-payment-history", UserAuth, PaymentController.getPaymentHistory);

module.exports = router;